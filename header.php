<?php
require_once "action.php";
$current_url = explode('/',$_SERVER['HTTP_HOST'] . $_SERVER['PHP_SELF']);
//get categories
$getCategories = $db->select()->from('categories')->where('status','=','1')->where('parent_id','=',0)->orderBy('title','ASC');
$getCategories = $getCategories->execute();
$getCategories = $getCategories->fetchAll(PDO::FETCH_ASSOC);

$getBrands = $db->select()->from('brands')->where('status','=','1')->orderBy('title','ASC');
$getBrands = $getBrands->execute();
$getBrands = $getBrands->fetchAll(PDO::FETCH_ASSOC);

?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8" />
    <meta name="format-detection" content="telephone=no" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link href="assets/image/favicon.png" rel="icon" />
    <title><?php echo str_replace('-',' ',end($current_url)) ?> - ECommerce Online Store</title>
    <meta name="description" content="Responsive and clean html template design for any kind of ecommerce webshop">
    <!-- CSS Part Start-->
    <link rel="stylesheet" type="text/css" href="assets/js/bootstrap/css/bootstrap.min.css" />
    <link rel="stylesheet" type="text/css" href="assets/css/font-awesome/css/font-awesome.min.css" />
    <link rel="stylesheet" type="text/css" href="assets/css/stylesheet.css" />
    <link rel="stylesheet" type="text/css" href="assets/css/owl.carousel.css" />
    <link rel="stylesheet" type="text/css" href="assets/css/owl.transitions.css" />
    <link rel="stylesheet" type="text/css" href="assets/js/swipebox/src/css/swipebox.min.css">
    <link rel="stylesheet" type="text/css" href="assets/css/responsive.css" />
    <link rel="stylesheet" type="text/css" href="assets/css/stylesheet-skin3.css" />
    <link href='http://fonts.googleapis.com/css?family=Droid+Sans' rel='stylesheet' type='text/css'>
    <!-- CSS Part End-->
</head>
<body>
<div class="wrapper-wide">
    <div id="header">
        <!-- Top Bar Start-->
        <nav id="top" class="htop">
            <div class="container">
                <div class="row"> <span class="drop-icon visible-sm visible-xs"><i class="fa fa-align-justify"></i></span>
                    <div class="pull-left flip left-top">
                        <div class="links">
                            <ul>
                                <li class="mobile"><i class="fa fa-phone"></i>+91 9898777656</li>
                                <li class="email"><a href="mailto:danish.imtiaz@cubixlabs.com"><i class="fa fa-envelope"></i>info@marketshop.com</a></li>
                                <li><a href="wishlist.php">Wish List (0)</a></li>
                            </ul>
                        </div>
                    </div>
                    <div id="top-links" class="nav pull-right flip">
                        <ul>
                          <?php
                          if(isset($_COOKIE['current_user'])){
                              echo '<li><a href="account_setting.php">Welcome: '. $getUser['first_name'] .'</a></li>';
                              echo '<li><a href="'. $_SERVER['PHP_SELF'] .'?logout=true">Logout</a></li>';
                          }else{
                            echo '<li><a href="login.php">Login</a></li>';
                            echo '<li><a href="register.php">Register</a></li>';
                          }
                          ?>
                        </ul>
                    </div>
                </div>
            </div>
        </nav>
        <!-- Top Bar End-->
        <!-- Header Start-->
        <header class="header-row">
            <div class="container">
                <div class="table-container">
                    <!-- Logo Start -->
                    <div class="col-table-cell col-lg-4 col-md-4 col-sm-12 col-xs-12 inner">
                        <div id="logo"><a href="assets/index.html"><img class="img-responsive" src="assets/image/logo.png" title="MarketShop" alt="MarketShop" /></a></div>
                    </div>
                    <!-- Logo End -->
                    <!-- Search Start-->
                    <div class="col-table-cell col-lg-5 col-md-5 col-md-push-0 col-sm-6 col-sm-push-6 col-xs-12">
                        <div id="search" class="input-group">
                            <input id="filter_name" type="text" name="search" value="" placeholder="Search" class="form-control input-lg" />
                            <button type="button" class="button-search"><i class="fa fa-search"></i></button>
                        </div>
                    </div>
                    <!-- Search End-->
                    <!-- Mini Cart Start-->
                    <div class="col-table-cell col-lg-3 col-md-3 col-md-pull-0 col-sm-6 col-sm-pull-6 col-xs-12 inner">
                        <div id="cart">
                            <button type="button" data-toggle="dropdown" data-loading-text="Loading..." class="heading dropdown-toggle"> <span class="cart-icon pull-left flip"></span> <span id="cart-total">2 item(s) - $1,132.00</span></button>
                            <ul class="dropdown-menu">
                                <li>
                                    <table class="table">
                                        <tbody>
                                        <tr>
                                            <td class="text-center"><a href="assets/product.html"><img class="img-thumbnail" title="Xitefun Causal Wear Fancy Shoes" alt="Xitefun Causal Wear Fancy Shoes" src="assets/image/product/sony_vaio_1-50x50.jpg"></a></td>
                                            <td class="text-left"><a href="assets/product.html">Xitefun Causal Wear Fancy Shoes</a></td>
                                            <td class="text-right">x 1</td>
                                            <td class="text-right">$902.00</td>
                                            <td class="text-center"><button class="btn btn-danger btn-xs remove" title="Remove" onClick="" type="button"><i class="fa fa-times"></i></button></td>
                                        </tr>
                                        <tr>
                                            <td class="text-center"><a href="assets/product.html"><img class="img-thumbnail" title="Aspire Ultrabook Laptop" alt="Aspire Ultrabook Laptop" src="assets/image/product/samsung_tab_1-50x50.jpg"></a></td>
                                            <td class="text-left"><a href="assets/product.html">Aspire Ultrabook Laptop</a></td>
                                            <td class="text-right">x 1</td>
                                            <td class="text-right">$230.00</td>
                                            <td class="text-center"><button class="btn btn-danger btn-xs remove" title="Remove" onClick="" type="button"><i class="fa fa-times"></i></button></td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </li>
                                <li>
                                    <div>
                                        <table class="table table-bordered">
                                            <tbody>
                                            <tr>
                                                <td class="text-right"><strong>Sub-Total</strong></td>
                                                <td class="text-right">$940.00</td>
                                            </tr>
                                            <tr>
                                                <td class="text-right"><strong>Eco Tax (-2.00)</strong></td>
                                                <td class="text-right">$4.00</td>
                                            </tr>
                                            <tr>
                                                <td class="text-right"><strong>VAT (20%)</strong></td>
                                                <td class="text-right">$188.00</td>
                                            </tr>
                                            <tr>
                                                <td class="text-right"><strong>Total</strong></td>
                                                <td class="text-right">$1,132.00</td>
                                            </tr>
                                            </tbody>
                                        </table>
                                        <p class="checkout"><a href="cart.php" class="btn btn-primary"><i class="fa fa-shopping-cart"></i> View Cart</a>&nbsp;&nbsp;&nbsp;<a href="checkout.php" class="btn btn-primary"><i class="fa fa-share"></i> Checkout</a></p>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <!-- Mini Cart End-->
                </div>
            </div>
        </header>
        <!-- Header End-->
        <!-- Main Menu Start-->
        <nav id="menu" class="navbar">
            <div class="container">
                <div class="navbar-header"> <span class="visible-xs visible-sm"> Menu <b></b></span></div>
                <div class="collapse navbar-collapse navbar-ex1-collapse">
                    <ul class="nav navbar-nav">
                        <li><a class="home_link" title="Home" href="index.php"><span>Home</span></a></li>
                        <li class="dropdown"><a>Categories</a>
                            <div class="dropdown-menu">
                                <ul>
                                    <?php
                                        if(!empty($getCategories)){
                                            foreach ($getCategories as $category){
                                    ?>
                                            <li> <a href="products.php?category=<?php echo $category['slug'] ?>"><?php echo $category['title'] ?><span>&rsaquo;</span></a>
                                                <?php
                                                    $getSubCategories = $db->select()->from('categories')->where('status','=','1')->where('parent_id','=',$category['id'])->orderBy('title','ASC');
                                                    $getSubCategories = $getSubCategories->execute();
                                                    $getSubCategories = $getSubCategories->fetchAll(PDO::FETCH_ASSOC);
                                                    if(!empty($getSubCategories)) {
                                                        echo '<div class="dropdown-menu">';
                                                        foreach ($getSubCategories as $subCategory){
                                                ?>
                                                                <ul>
                                                                    <li><a href="products.php?category=<?php echo $category['slug'] ?>&sub-category=<?php echo $subCategory['slug'] ?>"><?php echo $subCategory['title'] ?><span>&rsaquo;</span></a>
                                                                        <div class="dropdown-menu">
                                                                        <?php
                                                                            $sql = "SELECT b.* FROM brand_category bc 
                                                                                    INNER JOIN brands b ON b.id = bc.brand_id
                                                                                    WHERE bc.category_id = ". $subCategory['id'] ." AND status = '1' 
                                                                                    GROUP BY b.id ORDER By b.title ASC";
                                                                            $getCategoryBrands = $db->query($sql)->fetchAll(PDO::FETCH_ASSOC);
                                                                            if(!empty($getCategoryBrands)){
                                                                                foreach($getCategoryBrands as $categoryBrands){
                                                                        ?>
                                                                            <ul>
                                                                                <li><a href="products.php?category=<?php echo $category['slug'] ?>&sub-category=<?php echo $subCategory['slug'] ?>&brand=<?php echo $categoryBrands['slug'] ?>"><?php echo $categoryBrands['title'] ?></a></li>
                                                                            </ul>
                                                                        <?php } } ?>
                                                                        </div>
                                                                    </li>
                                                                </ul>

                                                 <?php
                                                        }
                                                        echo '</div>';
                                                    }
                                                ?>
                                            </li>
                                    <?php
                                            }
                                        }
                                    ?>
                                </ul>
                            </div>
                        </li>
                        <li class="menu_brands dropdown"><a>Brands</a>
                            <?php
                                if(!empty($getBrands)) {
                                    echo '<div class="dropdown-menu">';
                                    foreach($getBrands as $brand) {
                            ?>
                            <div class="col-lg-1 col-md-2 col-sm-3 col-xs-6">
                                <a href="products.php?brand=<?php echo $brand['slug'] ?>">
                                    <img style="" src="_uploads/<?php echo $brand['image'] ?>" title="<?php echo $brand['title'] ?>" alt="<?php echo $brand['title'] ?>"/>
                                </a>
                                <a href="products.php?brand=<?php echo $brand['slug'] ?>">
                                    <?php echo $brand['title'] ?>
                                </a>
                            </div>
                            <?php
                                    }
                                    echo ' </div>';
                                }
                            ?>
                        </li>
                        <li class="custom-link"><a href="products.php">Products</a></li>
                        <li class="custom-link"><a href="about-us.php">About US</a></li>
                        <li class="contact-link"><a href="contact-us.php">Contact Us</a></li>
                    </ul>
                </div>
            </div>
        </nav>
        <!-- Main Menu End-->
    </div>
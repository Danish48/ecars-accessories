/*
SQLyog Ultimate - MySQL GUI v8.2 
MySQL - 5.6.14 : Database - php_project
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`php_project` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `php_project`;

/*Table structure for table `add_to_cart` */

DROP TABLE IF EXISTS `add_to_cart`;

CREATE TABLE `add_to_cart` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `session_id` varchar(255) DEFAULT NULL,
  `product_id` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `FK_add_to_cart` (`user_id`),
  KEY `FK_add_to_cart_product_id` (`product_id`),
  CONSTRAINT `FK_add_to_cart` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE,
  CONSTRAINT `FK_add_to_cart_product_id` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `add_to_cart` */

/*Table structure for table `brand_category` */

DROP TABLE IF EXISTS `brand_category`;

CREATE TABLE `brand_category` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `category_id` int(11) NOT NULL,
  `brand_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `id_idx` (`category_id`),
  KEY `brand_id_idx` (`brand_id`),
  CONSTRAINT `brand_id` FOREIGN KEY (`brand_id`) REFERENCES `brands` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `id` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

/*Data for the table `brand_category` */

insert  into `brand_category`(`id`,`category_id`,`brand_id`,`created_at`) values (3,33,16,'2018-02-09 21:49:08'),(4,8,17,'2018-02-09 21:49:40'),(5,31,18,'2018-02-09 21:50:13'),(6,28,19,'2018-02-09 21:50:41'),(7,29,20,'2018-02-09 21:51:07');

/*Table structure for table `brands` */

DROP TABLE IF EXISTS `brands`;

CREATE TABLE `brands` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `title` varchar(55) DEFAULT NULL,
  `slug` varchar(55) DEFAULT NULL,
  `image` varchar(55) DEFAULT NULL,
  `description` text,
  `status` enum('0','1') DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `FK_brands` (`user_id`),
  CONSTRAINT `FK_brands` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=latin1;

/*Data for the table `brands` */

insert  into `brands`(`id`,`user_id`,`title`,`slug`,`image`,`description`,`status`,`created_at`) values (16,11,'Ford','Ford','1518194948.png','Lorem Ipsum is simply dummy text of the printing and typesetting industry','1','2018-02-09 21:49:08'),(17,11,'BMW','BMW','1518194980.png','Lorem Ipsum is simply dummy text of the printing and typesetting industry','1','2018-02-09 21:49:40'),(18,11,'Ferrari','Ferrari','1518195013.png','Lorem Ipsum is simply dummy text of the printing and typesetting industry','1','2018-02-09 21:50:13'),(19,11,'Honda','Honda','1518195041.png','Lorem Ipsum is simply dummy text of the printing and typesetting industry','1','2018-02-09 21:50:41'),(20,11,'Audi','Audi','1518195067.png','Lorem Ipsum is simply dummy text of the printing and typesetting industry','1','2018-02-09 21:51:07');

/*Table structure for table `categories` */

DROP TABLE IF EXISTS `categories`;

CREATE TABLE `categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_id` varchar(55) NOT NULL DEFAULT '0',
  `user_id` int(11) NOT NULL,
  `title` varchar(55) NOT NULL,
  `slug` varchar(55) NOT NULL,
  `image` varchar(55) DEFAULT NULL,
  `description` text,
  `status` enum('0','1') NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `FK_categories` (`user_id`),
  CONSTRAINT `FK_categories` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=35 DEFAULT CHARSET=latin1;

/*Data for the table `categories` */

insert  into `categories`(`id`,`parent_id`,`user_id`,`title`,`slug`,`image`,`description`,`status`,`created_at`) values (4,'3',1,'Mobile','Mobile',NULL,'','1','2018-02-03 17:13:47'),(5,'3',2,'Laptop','Laptop',NULL,'','1','2018-02-07 00:46:21'),(6,'3',2,'Camera','Camera',NULL,'','1','2018-02-07 00:47:01'),(7,'3',2,'TV & Home Audio','TV-Home-Audio',NULL,'','1','2018-02-07 00:47:30'),(8,'3',2,'Audio & Video player','Audio-Video-player',NULL,'','1','2018-02-07 00:48:05'),(13,'9',2,'Men Shoes','Men-Shoes',NULL,'','1','2018-02-07 00:57:33'),(15,'9',2,'Women Shoes','Women-Shoes',NULL,'','1','2018-02-07 01:00:10'),(16,'10',2,'Men Watches','Men-Watches',NULL,'','1','2018-02-07 01:00:49'),(17,'10',2,'Ladies Watches','Ladies-Watches',NULL,'','1','2018-02-07 01:01:05'),(18,'11',2,'Silver','Silver',NULL,'','1','2018-02-07 01:03:36'),(19,'11',2,'Gold','Gold',NULL,'','1','2018-02-07 01:03:46'),(20,'12',2,'Cycling','Cycling',NULL,'','1','2018-02-07 01:04:47'),(21,'12',2,'Running','Running',NULL,'','1','2018-02-07 01:04:58'),(22,'12',2,'Football','Football',NULL,'','1','2018-02-07 01:05:35'),(23,'12',2,'Cricket','Cricket',NULL,'','1','2018-02-07 01:05:43'),(24,'',11,'Batteries, Starting And Charging','Batteries-Starting-And-Charging',NULL,'You expect your engine to startup with ease every time you slide into your vehicle. Unfortunately, if your battery, starter or alternator isn’t up to par, chances are your vehicle is going to struggle to fire up quickly. Luckily, AutoZone has every component you need when it comes to your charging system.','1','2018-02-09 21:39:46'),(25,'',11,'Brakes And Traction Control','Brakes-And-Traction-Control',NULL,'Whether you\'re hitting the gas or slowing at a stop sign, you expect your traction control to be hard at work. Paired with your brake system, these important components make sure you and your passengers are safe every time you get behind the wheel. AutoZone supplies the best brakes and traction control systems for your vehicle, to make staying on the road a cinch.','1','2018-02-09 21:40:38'),(26,'',11,'Fuel Delivery','Fuel-Delivery',NULL,'All the gas in the world still won\'t make your vehicle move without a functioning fuel delivery system. So, make sure you\'re ready to roll at a moment\'s notice with properly pumping fuel lines and fuel filters. AutoZone\'s replacement fuel systems reliably feed your ride with the appropriate amount of gas or diesel, saving you from an inoperable engine.','1','2018-02-09 21:40:55'),(27,'',11,'Interior','Interior',NULL,'','1','2018-02-09 21:41:26'),(28,'',11,'Truck And Towing','Truck-And-Towing',NULL,'Just like your boots were made for walkin\', your truck was made for haulin\'. So make sure it\'s equipped with the towing and protective gear it needs to get the job done. Inside and outside, front to back, and top to bottom, AutoZone has everything you need to trick out your truck.','1','2018-02-09 21:41:43'),(29,'25',11,'Brakes','Brakes',NULL,'Lorem Ipsum is simply dummy text of the printing and typesetting industry','1','2018-02-09 21:43:31'),(30,'25',11,'Brake Drum All-In-One Kit - Rear Brake Drum','Brake-Drum-All-In-One-Kit---Rear-Brake-Drum',NULL,'Lorem Ipsum is simply dummy text of the printing and typesetting industry','1','2018-02-09 21:44:14'),(31,'26',11,'Fuel Tank','Fuel-Tank',NULL,'Lorem Ipsum is simply dummy text of the printing and typesetting industry','1','2018-02-09 21:44:58'),(32,'26',11,'Home  Parts  Fuel Delivery Fuel Tank And Sending Unit F','Home-Parts-Fuel-Delivery-Fuel-Tank-And-Sending-Unit-Fue',NULL,'Lorem Ipsum is simply dummy text of the printing and typesetting industry','1','2018-02-09 21:45:36'),(33,'27',11,' Seat Cover','-Seat-Cover',NULL,'Lorem Ipsum is simply dummy text of the printing and typesetting industry','1','2018-02-09 21:46:16'),(34,'27',11,'Dash Cover - Front','Dash-Cover---Front',NULL,'Lorem Ipsum is simply dummy text of the printing and typesetting industry','1','2018-02-09 21:46:53');

/*Table structure for table `order` */

DROP TABLE IF EXISTS `order`;

CREATE TABLE `order` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `fullname` varchar(45) DEFAULT NULL,
  `email` varchar(45) DEFAULT NULL,
  `mobile_no` varchar(45) DEFAULT NULL,
  `billing_address` text,
  `order_price` float DEFAULT NULL,
  `status` enum('pending','delivered','canceled','transit') NOT NULL DEFAULT 'pending',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `FK_order` (`user_id`),
  CONSTRAINT `FK_order` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `order` */

/*Table structure for table `order_meta` */

DROP TABLE IF EXISTS `order_meta`;

CREATE TABLE `order_meta` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `order_id` int(11) DEFAULT NULL,
  `product_id` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `FK_order_meta` (`order_id`),
  KEY `FK_order_meta_product_id` (`product_id`),
  CONSTRAINT `FK_order_meta` FOREIGN KEY (`order_id`) REFERENCES `order` (`id`) ON DELETE CASCADE,
  CONSTRAINT `FK_order_meta_product_id` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `order_meta` */

/*Table structure for table `product_category` */

DROP TABLE IF EXISTS `product_category`;

CREATE TABLE `product_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `FK_product_category` (`product_id`),
  KEY `FK_product_category_id` (`category_id`),
  CONSTRAINT `FK_product_category` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE,
  CONSTRAINT `FK_product_category_id` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

/*Data for the table `product_category` */

insert  into `product_category`(`id`,`product_id`,`category_id`,`created_at`) values (6,14,33,'2018-02-09 21:56:52'),(7,15,31,'2018-02-09 21:58:10'),(8,16,34,'2018-02-09 21:59:03'),(9,17,30,'2018-02-09 22:00:04');

/*Table structure for table `product_gallery` */

DROP TABLE IF EXISTS `product_gallery`;

CREATE TABLE `product_gallery` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) NOT NULL,
  `image` varchar(55) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `FK_product_gallery` (`product_id`),
  CONSTRAINT `FK_product_gallery` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=latin1;

/*Data for the table `product_gallery` */

insert  into `product_gallery`(`id`,`product_id`,`image`,`created_at`) values (10,14,'01518195412.png','2018-02-09 21:56:52'),(11,15,'01518195490.png','2018-02-09 21:58:10'),(12,15,'11518195490.png','2018-02-09 21:58:10'),(13,16,'01518195543.png','2018-02-09 21:59:03'),(14,16,'11518195543.png','2018-02-09 21:59:03'),(15,16,'21518195543.png','2018-02-09 21:59:03'),(16,16,'31518195543.png','2018-02-09 21:59:03'),(17,16,'41518195543.png','2018-02-09 21:59:03'),(18,16,'51518195543.png','2018-02-09 21:59:03'),(19,16,'61518195543.png','2018-02-09 21:59:03'),(20,16,'71518195543.png','2018-02-09 21:59:03'),(21,16,'81518195543.png','2018-02-09 21:59:03'),(22,16,'91518195543.png','2018-02-09 21:59:03'),(23,16,'101518195543.png','2018-02-09 21:59:03'),(24,17,'01518195604.png','2018-02-09 22:00:04');

/*Table structure for table `products` */

DROP TABLE IF EXISTS `products`;

CREATE TABLE `products` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `title` varchar(55) DEFAULT NULL,
  `slug` varchar(55) DEFAULT NULL,
  `image` varchar(55) DEFAULT NULL,
  `description` text,
  `short_description` text,
  `sale_price` float DEFAULT NULL,
  `regular_price` float DEFAULT NULL,
  `status` enum('0','1') DEFAULT '1',
  `stock_status` enum('0','1') DEFAULT '1',
  `brand_id` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `FK_products` (`user_id`),
  KEY `FK_products_brand_id` (`brand_id`),
  CONSTRAINT `FK_products` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE,
  CONSTRAINT `FK_products_brand_id` FOREIGN KEY (`brand_id`) REFERENCES `brands` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=latin1;

/*Data for the table `products` */

insert  into `products`(`id`,`user_id`,`title`,`slug`,`image`,`description`,`short_description`,`sale_price`,`regular_price`,`status`,`stock_status`,`brand_id`,`created_at`) values (14,12,'audi seats covers','audi-seats-covers','1518195412.jpg','%3Cp%3E%3Cstrong%3ELorem+Ipsum%3C%2Fstrong%3E%26nbsp%3Bis+simply+dummy+text+of+the+printing+and+typesetting+industry.%3C%2Fp%3E%0D%0A','Lorem Ipsum is simply dummy text of the printing and typesetting industry.',250,300,'1','1',20,'2018-02-09 21:56:52'),(15,12,'BMW fuel systems','BMW-fuel-systems','1518195490.png','%3Cp%3E%3Cstrong%3ELorem+Ipsum%3C%2Fstrong%3E%26nbsp%3Bis+simply+dummy+text+of+the+printing+and+typesetting+industry.%3C%2Fp%3E%0D%0A','Lorem Ipsum is simply dummy text of the printing and typesetting industry.',250,300,'1','1',17,'2018-02-09 21:58:10'),(16,12,'ferrari covers','ferrari-covers','1518195543.png','%3Cp%3E%3Cstrong%3ELorem+Ipsum%3C%2Fstrong%3E%26nbsp%3Bis+simply+dummy+text+of+the+printing+and+typesetting+industry.%3C%2Fp%3E%0D%0A','Lorem Ipsum is simply dummy text of the printing and typesetting industry.',250,300,'1','1',18,'2018-02-09 21:59:03'),(17,12,'Ford Car brakes and drums','Ford-Car-brakes-and-drums','1518195604.png','%3Cp%3E%3Cstrong%3ELorem+Ipsum%3C%2Fstrong%3E%26nbsp%3Bis+simply+dummy+text+of+the+printing+and+typesetting+industry.%3C%2Fp%3E%0D%0A','Lorem Ipsum is simply dummy text of the printing and typesetting industry.',250,300,'1','1',16,'2018-02-09 22:00:04');

/*Table structure for table `slider` */

DROP TABLE IF EXISTS `slider`;

CREATE TABLE `slider` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(45) DEFAULT NULL,
  `image` varchar(45) DEFAULT NULL,
  `description` varchar(45) DEFAULT NULL,
  `link` varchar(255) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

/*Data for the table `slider` */

insert  into `slider`(`id`,`title`,`image`,`description`,`link`,`created_at`) values (3,'Slider 1','1517950299.jpg','','','2018-02-07 01:51:39'),(4,'Slider 2','1517950312.jpg','','','2018-02-07 01:51:52'),(5,'Slider 3','1517950323.jpg','','','2018-02-07 01:52:03');

/*Table structure for table `user_meta` */

DROP TABLE IF EXISTS `user_meta`;

CREATE TABLE `user_meta` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `meta_key` varchar(55) DEFAULT NULL,
  `meta_description` text,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `FK_user_meta` (`user_id`),
  CONSTRAINT `FK_user_meta` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

/*Data for the table `user_meta` */

insert  into `user_meta`(`id`,`user_id`,`meta_key`,`meta_description`,`created_at`) values (2,1,'_mobile','0324-2744356','2017-11-04 01:33:22'),(3,2,'_mobile','0332-2787244','2018-02-04 17:16:43'),(4,2,'_address','test addresss','2018-02-04 17:16:43'),(5,10,'_mobile','3242744355','2018-02-09 01:24:09'),(6,10,'_address','this testing address','2018-02-09 01:24:09');

/*Table structure for table `users` */

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(55) DEFAULT NULL,
  `last_name` varchar(55) DEFAULT NULL,
  `email` varchar(55) DEFAULT NULL,
  `password` varchar(55) DEFAULT NULL,
  `role` enum('user','admin','vendor') NOT NULL DEFAULT 'user',
  `status` enum('0','1') DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

/*Data for the table `users` */

insert  into `users`(`id`,`first_name`,`last_name`,`email`,`password`,`role`,`status`,`created_at`) values (1,'raza','nasir','raza.nasir856@gmail.com','e6e061838856bf47e1de730719fb2609','admin','1','2017-11-04 01:33:22'),(2,'raza','nasir','razanasir856@yahoo.com','e6e061838856bf47e1de730719fb2609','vendor','1','2017-11-04 01:33:22'),(10,'raza','nasir','raza.nasir856@gmail.com','25d55ad283aa400af464c76d713c07ad','user','1','2018-02-08 01:40:07'),(11,'danish','imtiaz','mdanishimtiaz@gmail.com','e6e061838856bf47e1de730719fb2609','admin','1','2018-02-09 20:29:21'),(12,'Muhammad Danish','Ahmed','danish.imtiaz@cubixlabs.com','e6e061838856bf47e1de730719fb2609','vendor','1','2018-02-09 17:52:55');

/*Table structure for table `wishlist` */

DROP TABLE IF EXISTS `wishlist`;

CREATE TABLE `wishlist` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `product_id` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `FK_wishlist` (`user_id`),
  KEY `FK_wishlist_product_id` (`product_id`),
  CONSTRAINT `FK_wishlist` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE,
  CONSTRAINT `FK_wishlist_product_id` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `wishlist` */

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

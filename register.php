<?php
  if(isset($_COOKIE['current_user'])){
     header("Location:index.php");
     die();
  }
  require_once "header.php";
?>
  <div id="container">
    <div class="container">
      <div class="row">
        <!--Middle Part Start-->
        <?php require_once "flash_message.php"; ?>
        <form method="post" action="<?php echo $_SERVER['PHP_SELF'] ?>">
          <input type="hidden" name="register" value="1">
          <div id="content" class="col-sm-9">
          <h1 class="title">Register</h1>
          <div class="row">
            <div class="col-sm-6">
              <div class="form-group">
                <label class="control-label" for="input-email">First Name<span class="text-danger">*</span></label>
                <input type="text" name="first_name" value="" placeholder="First Name" id="input-email" class="form-control" />
              </div>
              <div class="form-group">
                <label class="control-label" for="input-email">Last Name<span class="text-danger">*</span></label>
                <input type="text" name="last_name" value="" placeholder="Last Name" id="input-email" class="form-control" />
              </div>
              <div class="form-group">
                <label class="control-label" for="input-email">E-Mail Address<span class="text-danger">*</span></label>
                <input type="text" name="email" value="" placeholder="E-Mail Address" id="input-email" class="form-control" />
              </div>
              <div class="form-group">
                <label class="control-label" for="input-email">Password<span class="text-danger">*</span></label>
                <input type="password" name="password" value="" placeholder="password" id="input-email" class="form-control" />
              </div>
              <div class="form-group">
                <label class="control-label" for="input-password">Confirm Password<span class="text-danger">*</span></label>
                <input type="password" name="confirm_password" value="" placeholder="Confirm Password" id="input-password" class="form-control" />
              </div>
              <div class="form-group">
                <label class="control-label" for="input-email">Role<span class="text-danger">*</span></label>
                <select name="role" class="form-control">
                  <option value="user">User</option>
                  <option value="vendor">Vendor</option>
                </select>
              </div>
              <input type="submit" value="Register" class="btn btn-primary" />
            </div>
          </div>
        </div>
        </form>
      </div>
    </div>
  </div>
<?php
  require_once "footer.php";
?>

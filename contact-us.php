<?php 
  require_once "header.php";
?>
  <div id="container">
    <div class="container">
      <div class="row">
        <!--Middle Part Start-->
        <?php require_once "flash_message.php" ?>
        <div id="content" class="col-lg-8 col-offset-lg-2 col-md-8 col-offset-md-2 col-sm-8 col-offset-sm-2 col-xl-8 col-offset-xl-2">
          <h1 class="title">Contact Us</h1>
          <form method="post" class="form-horizontal">
            <input type="hidden" name="contact_us" value="1">
            <fieldset>
              <div class="form-group required">            
                <div class="col-md-6 col-sm-6">
                  <input type="text" name="name" value="" id="input-name" class="form-control" placeholder="Your Name" />
                </div>
              </div>
              <div class="form-group required">
                <div class="col-md-6 col-sm-6">
                  <input type="text" name="email" value="" id="input-email" class="form-control" placeholder="E-Mail Address" />
                </div>
              </div>
              <div class="form-group required">
                <div class="col-md-10 col-sm-">
                  <textarea name="enquiry" rows="6" id="input-enquiry" class="form-control" placeholder="Enquiry"></textarea>
                </div>
              </div>
            </fieldset>
            <div class="buttons">
              <div class="pull-right">
                <input class="btn btn-primary" type="submit" value="Submit" />
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
<?php 
  require_once "footer.php";
?>
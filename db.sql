-- MySQL dump 10.13  Distrib 5.7.20, for Linux (x86_64)
--
-- Host: localhost    Database: php_project
-- ------------------------------------------------------
-- Server version	5.7.20-0ubuntu0.16.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `add_to_cart`
--

DROP TABLE IF EXISTS `add_to_cart`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `add_to_cart` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `session_id` varchar(255) DEFAULT NULL,
  `product_id` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `FK_add_to_cart` (`user_id`),
  KEY `FK_add_to_cart_product_id` (`product_id`),
  CONSTRAINT `FK_add_to_cart` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE,
  CONSTRAINT `FK_add_to_cart_product_id` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `add_to_cart`
--

LOCK TABLES `add_to_cart` WRITE;
/*!40000 ALTER TABLE `add_to_cart` DISABLE KEYS */;
/*!40000 ALTER TABLE `add_to_cart` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `brand_category`
--

DROP TABLE IF EXISTS `brand_category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `brand_category` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `category_id` int(11) NOT NULL,
  `brand_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `id_idx` (`category_id`),
  KEY `brand_id_idx` (`brand_id`),
  CONSTRAINT `brand_id` FOREIGN KEY (`brand_id`) REFERENCES `brands` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `id` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `brand_category`
--

LOCK TABLES `brand_category` WRITE;
/*!40000 ALTER TABLE `brand_category` DISABLE KEYS */;
INSERT INTO `brand_category` VALUES (1,6,15,'2018-02-08 21:45:48'),(2,23,15,'2018-02-08 21:45:48');
/*!40000 ALTER TABLE `brand_category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `brands`
--

DROP TABLE IF EXISTS `brands`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `brands` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `title` varchar(55) DEFAULT NULL,
  `slug` varchar(55) DEFAULT NULL,
  `image` varchar(55) DEFAULT NULL,
  `description` text,
  `status` enum('0','1') DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `FK_brands` (`user_id`),
  CONSTRAINT `FK_brands` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `brands`
--

LOCK TABLES `brands` WRITE;
/*!40000 ALTER TABLE `brands` DISABLE KEYS */;
INSERT INTO `brands` VALUES (2,1,'Nokia','Nokia','1517655564.png','nokia','1','2018-02-03 10:59:24'),(6,2,'Dell','Dell','1517945902.png','','1','2018-02-06 19:38:22'),(7,2,'Intel','Intel','1517945912.png','','1','2018-02-06 19:38:32'),(8,2,'Samsung','Samsung','1517945930.png','','1','2018-02-06 19:38:50'),(9,2,'Asus','Asus','1517945946.png','','1','2018-02-06 19:39:06'),(10,2,'Sony','Sony','1517945964.png','','1','2018-02-06 19:39:24'),(11,2,'HP','HP','1517946132.png','','1','2018-02-06 19:42:12'),(12,2,'Apple','Apple','1517946149.png','','1','2018-02-06 19:42:29'),(13,2,'Lenovo','Lenovo','1517946172.png','','1','2018-02-06 19:42:52'),(14,2,'LG','LG','1517946264.png','','1','2018-02-06 19:44:24'),(15,1,'test','test','1518126348.jpg','','1','2018-02-08 21:45:48');
/*!40000 ALTER TABLE `brands` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `categories`
--

DROP TABLE IF EXISTS `categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_id` varchar(55) NOT NULL DEFAULT '0',
  `user_id` int(11) NOT NULL,
  `title` varchar(55) NOT NULL,
  `slug` varchar(55) NOT NULL,
  `image` varchar(55) DEFAULT NULL,
  `description` text,
  `status` enum('0','1') NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `FK_categories` (`user_id`),
  CONSTRAINT `FK_categories` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `categories`
--

LOCK TABLES `categories` WRITE;
/*!40000 ALTER TABLE `categories` DISABLE KEYS */;
INSERT INTO `categories` VALUES (3,'0',1,'Electronics','Electronics',NULL,'','1','2018-02-03 11:57:22'),(4,'3',1,'Mobile','Mobile',NULL,'','1','2018-02-03 12:13:47'),(5,'3',2,'Laptop','Laptop',NULL,'','1','2018-02-06 19:46:21'),(6,'3',2,'Camera','Camera',NULL,'','1','2018-02-06 19:47:01'),(7,'3',2,'TV & Home Audio','TV-Home-Audio',NULL,'','1','2018-02-06 19:47:30'),(8,'3',2,'Audio & Video player','Audio-Video-player',NULL,'','1','2018-02-06 19:48:05'),(9,'',2,'Shoes','Shoes',NULL,'','1','2018-02-06 19:51:27'),(10,'',2,'watches','watches',NULL,'','1','2018-02-06 19:52:16'),(11,'',2,'Jewellery','Jewellery',NULL,'','1','2018-02-06 19:52:49'),(12,'',2,'Sports','Sports',NULL,'','1','2018-02-06 19:56:37'),(13,'9',2,'Men Shoes','Men-Shoes',NULL,'','1','2018-02-06 19:57:33'),(15,'9',2,'Women Shoes','Women-Shoes',NULL,'','1','2018-02-06 20:00:10'),(16,'10',2,'Men Watches','Men-Watches',NULL,'','1','2018-02-06 20:00:49'),(17,'10',2,'Ladies Watches','Ladies-Watches',NULL,'','1','2018-02-06 20:01:05'),(18,'11',2,'Silver','Silver',NULL,'','1','2018-02-06 20:03:36'),(19,'11',2,'Gold','Gold',NULL,'','1','2018-02-06 20:03:46'),(20,'12',2,'Cycling','Cycling',NULL,'','1','2018-02-06 20:04:47'),(21,'12',2,'Running','Running',NULL,'','1','2018-02-06 20:04:58'),(22,'12',2,'Football','Football',NULL,'','1','2018-02-06 20:05:35'),(23,'12',2,'Cricket','Cricket',NULL,'','1','2018-02-06 20:05:43');
/*!40000 ALTER TABLE `categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `order`
--

DROP TABLE IF EXISTS `order`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `order` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `fullname` varchar(45) DEFAULT NULL,
  `email` varchar(45) DEFAULT NULL,
  `mobile_no` varchar(45) DEFAULT NULL,
  `billing_address` text,
  `order_price` float DEFAULT NULL,
  `status` enum('pending','delivered','canceled','transit') NOT NULL DEFAULT 'pending',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `FK_order` (`user_id`),
  CONSTRAINT `FK_order` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `order`
--

LOCK TABLES `order` WRITE;
/*!40000 ALTER TABLE `order` DISABLE KEYS */;
/*!40000 ALTER TABLE `order` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `order_meta`
--

DROP TABLE IF EXISTS `order_meta`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `order_meta` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `order_id` int(11) DEFAULT NULL,
  `product_id` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `FK_order_meta` (`order_id`),
  KEY `FK_order_meta_product_id` (`product_id`),
  CONSTRAINT `FK_order_meta` FOREIGN KEY (`order_id`) REFERENCES `order` (`id`) ON DELETE CASCADE,
  CONSTRAINT `FK_order_meta_product_id` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `order_meta`
--

LOCK TABLES `order_meta` WRITE;
/*!40000 ALTER TABLE `order_meta` DISABLE KEYS */;
/*!40000 ALTER TABLE `order_meta` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `product_category`
--

DROP TABLE IF EXISTS `product_category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `product_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `FK_product_category` (`product_id`),
  KEY `FK_product_category_id` (`category_id`),
  CONSTRAINT `FK_product_category` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE,
  CONSTRAINT `FK_product_category_id` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `product_category`
--

LOCK TABLES `product_category` WRITE;
/*!40000 ALTER TABLE `product_category` DISABLE KEYS */;
INSERT INTO `product_category` VALUES (4,12,4,'2018-02-06 20:22:00'),(5,13,4,'2018-02-06 20:32:49');
/*!40000 ALTER TABLE `product_category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `product_gallery`
--

DROP TABLE IF EXISTS `product_gallery`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `product_gallery` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) NOT NULL,
  `image` varchar(55) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `FK_product_gallery` (`product_id`),
  CONSTRAINT `FK_product_gallery` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `product_gallery`
--

LOCK TABLES `product_gallery` WRITE;
/*!40000 ALTER TABLE `product_gallery` DISABLE KEYS */;
INSERT INTO `product_gallery` VALUES (2,12,'01517948520.jpg','2018-02-06 20:22:00'),(3,12,'11517948520.jpg','2018-02-06 20:22:00'),(4,12,'21517948520.jpeg','2018-02-06 20:22:00'),(5,12,'31517948521.png','2018-02-06 20:22:01'),(6,13,'01517948520.jpg','2018-02-06 20:33:36'),(7,13,'11517948520.jpg','2018-02-06 20:33:36'),(8,13,'21517948520.jpeg','2018-02-06 20:33:36'),(9,13,'31517948521.png','2018-02-06 20:33:36');
/*!40000 ALTER TABLE `product_gallery` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `products`
--

DROP TABLE IF EXISTS `products`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `products` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `title` varchar(55) DEFAULT NULL,
  `slug` varchar(55) DEFAULT NULL,
  `image` varchar(55) DEFAULT NULL,
  `description` text,
  `short_description` text,
  `sale_price` float DEFAULT NULL,
  `regular_price` float DEFAULT NULL,
  `status` enum('0','1') DEFAULT '1',
  `stock_status` enum('0','1') DEFAULT '1',
  `brand_id` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `FK_products` (`user_id`),
  KEY `FK_products_brand_id` (`brand_id`),
  CONSTRAINT `FK_products` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE,
  CONSTRAINT `FK_products_brand_id` FOREIGN KEY (`brand_id`) REFERENCES `brands` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `products`
--

LOCK TABLES `products` WRITE;
/*!40000 ALTER TABLE `products` DISABLE KEYS */;
INSERT INTO `products` VALUES (12,2,'Ipone 6s plus','Ipone-6s-plus','1517948520.jpeg','%3Cp%3EWhats+New%3Cstrong%3E%3Cspan+style%3D%22font-size%3A12px+%21important%22%3E%3Cspan+style%3D%22color%3A%230000ff+%21important%22%3E%3Cspan+style%3D%22background-color%3Atransparent+%21important%22%3E%3Cspan+style%3D%22font-family%3AArial%2CHelvetica%2Csans-serif+%21important%22%3EApple+Iphone+6%3C%2Fspan%3E%3C%2Fspan%3E%3C%2Fspan%3E%3C%2Fspan%3E%26nbsp%3BPlus+-%26nbsp%3B%3Cspan+style%3D%22font-size%3A12px+%21important%22%3E%3Cspan+style%3D%22color%3A%230000ff+%21important%22%3E%3Cspan+style%3D%22background-color%3Atransparent+%21important%22%3E%3Cspan+style%3D%22font-family%3AArial%2CHelvetica%2Csans-serif+%21important%22%3EBigger%3C%2Fspan%3E%3C%2Fspan%3E%3C%2Fspan%3E%3C%2Fspan%3E%26nbsp%3Band+Better.%3C%2Fstrong%3E%3Cbr+%2F%3E%0D%0A%3Cspan+style%3D%22font-size%3A12px+%21important%22%3E%3Cspan+style%3D%22color%3A%230000ff+%21important%22%3E%3Cspan+style%3D%22background-color%3Atransparent+%21important%22%3E%3Cspan+style%3D%22font-family%3AArial%2CHelvetica%2Csans-serif+%21important%22%3EChange%3C%2Fspan%3E%3C%2Fspan%3E%3C%2Fspan%3E%3C%2Fspan%3E%26nbsp%3Bis+inevitable%2C+Apple+plans+to+reveal%26nbsp%3Biphone%26nbsp%3B6s+Plus+and+being+one+of+the+most+iconic+products+of+the+company%2C+it+comes+with+many+upgrades.+With+rounded%26nbsp%3B%3Cspan+style%3D%22font-size%3A12px+%21important%22%3E%3Cspan+style%3D%22color%3A%230000ff+%21important%22%3E%3Cspan+style%3D%22background-color%3Atransparent+%21important%22%3E%3Cspan+style%3D%22font-family%3AArial%2CHelvetica%2Csans-serif+%21important%22%3Eedges%3C%2Fspan%3E%3C%2Fspan%3E%3C%2Fspan%3E%3C%2Fspan%3E%2C+Apple+has+created+a+masterpiece%2C+there+is+no%26nbsp%3B%3Cspan+style%3D%22font-size%3A12px+%21important%22%3E%3Cspan+style%3D%22color%3A%230000ff+%21important%22%3E%3Cspan+style%3D%22background-color%3Atransparent+%21important%22%3E%3Cspan+style%3D%22font-family%3AArial%2CHelvetica%2Csans-serif+%21important%22%3Edoubt%3C%2Fspan%3E%3C%2Fspan%3E%3C%2Fspan%3E%3C%2Fspan%3E%2C+wrapped+in+a+piece+of%26nbsp%3B%3Cspan+style%3D%22font-size%3A12px+%21important%22%3E%3Cspan+style%3D%22color%3A%230000ff+%21important%22%3E%3Cspan+style%3D%22background-color%3Atransparent+%21important%22%3E%3Cspan+style%3D%22font-family%3AArial%2CHelvetica%2Csans-serif+%21important%22%3Ealuminum%3C%2Fspan%3E%3C%2Fspan%3E%3C%2Fspan%3E%3C%2Fspan%3E%26nbsp%3Bthe%26nbsp%3B%3Cspan+style%3D%22font-size%3A12px+%21important%22%3E%3Cspan+style%3D%22color%3A%230000ff+%21important%22%3E%3Cspan+style%3D%22background-color%3Atransparent+%21important%22%3E%3Cspan+style%3D%22font-family%3AArial%2CHelvetica%2Csans-serif+%21important%22%3Eall+new%3C%2Fspan%3E%3C%2Fspan%3E%3C%2Fspan%3E%3C%2Fspan%3E%26nbsp%3B6s+ensures+a+comfort+fit.+Apple%27s+products+are+always+identical+in+design+and+only+differs+in%26nbsp%3B%3Cspan+style%3D%22font-size%3A12px+%21important%22%3E%3Cspan+style%3D%22color%3A%230000ff+%21important%22%3E%3Cspan+style%3D%22background-color%3Atransparent+%21important%22%3E%3Cspan+style%3D%22font-family%3AArial%2CHelvetica%2Csans-serif+%21important%22%3Eeither%3C%2Fspan%3E%3C%2Fspan%3E%3C%2Fspan%3E%3C%2Fspan%3E%26nbsp%3Bsize+of+iphones+with+increased%26nbsp%3B%3Cspan+style%3D%22font-size%3A12px+%21important%22%3E%3Cspan+style%3D%22color%3A%230000ff+%21important%22%3E%3Cspan+style%3D%22background-color%3Atransparent+%21important%22%3E%3Cspan+style%3D%22font-family%3AArial%2CHelvetica%2Csans-serif+%21important%22%3Elength%3C%2Fspan%3E%3C%2Fspan%3E%3C%2Fspan%3E%3C%2Fspan%3E%26nbsp%3B%26amp%3B+call+them+Plus+or%26nbsp%3B%3Cspan+style%3D%22font-size%3A12px+%21important%22%3E%3Cspan+style%3D%22color%3A%230000ff+%21important%22%3E%3Cspan+style%3D%22background-color%3Atransparent+%21important%22%3E%3Cspan+style%3D%22font-family%3AArial%2CHelvetica%2Csans-serif+%21important%22%3Eplacement%3C%2Fspan%3E%3C%2Fspan%3E%3C%2Fspan%3E%3C%2Fspan%3E%26nbsp%3Bof+iron+pieces+around+the+borders+and+bezels.+Apple%27s+new+smart+devices+could+be+a+real+eye-opener+for+who+want+a+real+smartphone%2C+as+6s+plus+is+the+phone+of+the+future.+A+stylish+plate+of%26nbsp%3Baluminum%26nbsp%3Band+glass+could%27ve+been+better%2C+if+the+design+department+had+visualized+6s+Plus+in+a+better+way.+Equipped+with+a+conventional+liquid+crystal+displays%2C+according+to+a+new+leak%2C+the+display+sizes+will+remain+the+same+with+5.5+inches.+Apple%27s+new+Force+Touch+Screen+and+pink+body+will+be+a+plus+for+your+selfie+sessions.+It+is+possible+to+add+new+colors+to%26nbsp%3Biphone%2C+in+addition+to+the+available+colors%2C+it+is+assumed+that+Apple%27s+interest+is+in+considering+the+release+of+a+pink+variation+along+with+space+grey%2C+sliver+and+gold.%26nbsp%3BiPhone%26nbsp%3B6s+Plus+production+have+already+started+but+it+should+be+taken+into+account+though%2C+that+it+will+take+some+time+to+get+released.+6s+plus+will+take+longer+to+reach+here+as+Apple+is+orphan+in+Pakistan.+like+we+all+know+that+there+is+no+authorized+dealer+of+the+company.+6s+Plus+in+local+market+would+cost+you+much+higher+than+it%27s+actual+cost+as+that+always+happens+here.+especially+to+the%26nbsp%3Biphonelovers.+Strong+and+powerful+novelty%2C+but+with+larger+screen.+Well+it+has+its+own+charisma+if+one+can+manage+such+a+size+from+Apple.+Rumored+to+be+loaded+with+a+sapphire+display+the+phablet+is+sure+to+make+some+waves+in+the+industry.%26nbsp%3B%26nbsp%3BBuildOSiOS+9+%26nbsp%3BDimensions158.1+x+77.8+x+7.1+mm+%26nbsp%3BWeight192+g+%26nbsp%3BSIMNano-SIM+%26nbsp%3BColorsSpace+Gray%2C+Silver%2C+Gold%2C+Rose+Gold%26nbsp%3B%26nbsp%3BFrequency2G+BandGSM+850+%2F+900+%2F+1800+%2F+1900+%26nbsp%3B3G+BandHSDPA+850+%2F+900+%2F+1700+%2F+1900+%2F+2100+%26nbsp%3B4G+BandLTE+band+1%282100%29%2C+2%281900%29%2C+3%281800%29%2C+4%281700%2F2100%29%2C+5%28850%29%2C+7%282600%29%2C+8%28900%29%2C+12%28700%29%2C+13%28700%29%2C+17%28700%29%2C+18%28800%29%2C+19%28800%29%2C+20%28800%29%2C+25%281900%29%2C+26%28850%29%2C+28%28700%29%2C+29%28700%29%2C+30%282300%29+-+A1633+%26nbsp%3BProcessorCPU%3Cstrong%3E2+GHz+Dual-Core%3C%2Fstrong%3E%26nbsp%3B%26nbsp%3BChipsetApple+A9+%26nbsp%3BDisplayTechnologyLED-backlit+IPS+LCD%2C+capacitive+touchscreen%2C+16M+colors%2C+multitouch+%26nbsp%3BSize5.5+inches+%26nbsp%3BResolution1080+x+1920+pixels+%28%7E401+ppi+pixel+density%29+%26nbsp%3BProtectionIon-Strengthened+Glass%2C+Sapphire+Crystal+Glass+with+Oleophobic+Coating+%26nbsp%3BExtra+Features3D+Touch+display%2C+Display+Zoom+%26nbsp%3BMemoryBuilt-in%3Cstrong%3E16%2F64%2F128GB%3C%2Fstrong%3E%26nbsp%3Bbuilt-in+%26nbsp%3BCardNo+%26nbsp%3BCameraMain12+MP%2C+4608+x+2592+pixels%2C+autofocus%2C+dual-LED+dual+tone+flash+%26nbsp%3BFeaturesOptical+image+stabilization%2C+phase+detection%2C+Geo-tagging%2C+simultaneous+HD+video+and+image+recording%2C+touch+focus%2C+face%2Fsmile+detection%2C+HDR+%28photo%2Fpanorama%29%2C+Video+1080p%4060fps%2C+720p%40240fps%2C+optical+stabilization+%26nbsp%3BFront5.0+MP+Camera+%26nbsp%3BConnectivityWLANWi-Fi+802.11+a%2Fb%2Fg%2Fn%2Fac%2C+dual-band%2C+hotspot+%26nbsp%3BBluetoothv4.1+with+A2DP%2C+LE+%26nbsp%3BGPSYes+%2B+A-GPS+support+%26amp%3B+Glonass+%26nbsp%3BRadioNo+%26nbsp%3BUSBUSB+%28v2.0%2C+reversible+connector%29+%26nbsp%3BNFCYes+%26nbsp%3BData%3Cspan+style%3D%22color%3A%23000077%22%3E%3Cstrong%3EGPRS%3C%2Fstrong%3E%3C%2Fspan%3E%2C%26nbsp%3B%3Cspan+style%3D%22color%3A%23000077%22%3E%3Cstrong%3EEDGE%3C%2Fstrong%3E%3C%2Fspan%3E%2C+3G+%28HSPA+42.2%2F5.76+Mbps%2C+4G+LTE+Cat4+150%2F50+Mbps%2C+EV-DO+Rev.A+3.1+Mbps+%26nbsp%3BFeaturesSensorsAccelerometer%2C+gyro%2C+proximity%2C+compass%2C+barometer%2C+Fingerprint+%26nbsp%3BAudio3.5mm+audio+jack%2C+Speakerphone+%26nbsp%3BBrowserHTML5+%28Safari%29+%26nbsp%3BMessagingiMessage%2C+SMS+%28threaded+view%29%2C+MMS%2C+Email%2C+Push+Email+%26nbsp%3BGamesBuilt-in+%2B+downloadable+%26nbsp%3BTorchYes+%26nbsp%3BExtraActive+noise+cancellation+with+dedicated+mic%2C+Document+editor%2C+Siri+natural+language+commands+and+dictation%2C+TV-out%2C+Photo%2Fvideo+editor%2C+Apple+Pay+%28Visa%2C+MasterCard%2C+AMEX+certified%29%2C+Voice+memo%2Fcommand%2Fdial%2C+Audio%2Fvideo+player+and+editor%2C+%26nbsp%3BTV-out%2C+iCloud+cloud+service+%26nbsp%3BBatteryCapacity%3Cstrong%3E2915+mAh%3C%2Fstrong%3E%26nbsp%3B%26nbsp%3BStandbyup+to+384+hrs+%26nbsp%3BTalktimeup+to+24+hrs+%26nbsp%3BMusicplayup+to+80+hrs+%26nbsp%3B%3C%2Fp%3E%0D%0A%0D%0A%3Ch2+style%3D%22margin-left%3A0px%3B+margin-right%3A0px%22%3E%3Cspan+style%3D%22font-size%3A10pt%22%3E%3Cspan+style%3D%22font-family%3AArial%2CHelvetica%2Csans-serif%22%3E%3Cspan+style%3D%22color%3A%23666666%22%3E%3Cstrong%3EPrice%3C%2Fstrong%3E%3C%2Fspan%3E%3C%2Fspan%3E%3C%2Fspan%3E%3C%2Fh2%3E%0D%0A%0D%0A%3Cp%3EPrice+in+Rs%3A%26nbsp%3B%3Cstrong%3E60%2C500%3C%2Fstrong%3E%26nbsp%3B%26nbsp%3B%26nbsp%3B%26nbsp%3BPrice+in+USD%3A%26nbsp%3B%3Cstrong%3E%24580%3C%2Fstrong%3E%26nbsp%3B%26nbsp%3BRatingsAverage+Rating+is%26nbsp%3B%3Cstrong%3E4.1+stars%3C%2Fstrong%3E%26nbsp%3B-+based+on%26nbsp%3B%3Cstrong%3E245%3C%2Fstrong%3E%26nbsp%3Buser+reviews.+%26nbsp%3B%3C%2Fp%3E%0D%0A','',60500,61500,'1','1',12,'2018-02-06 20:22:00'),(13,2,'Ipone 6 plus','Ipone-6-plus','1517948520.jpeg','<p>Whats New<strong><span style=\"font-size:12px !important\"><span style=\"color:#0000ff !important\"><span style=\"background-color:transparent !important\"><span style=\"font-family:Arial,Helvetica,sans-serif !important\">Apple Iphone 6</span></span></span></span>&nbsp;Plus -&nbsp;<span style=\"font-size:12px !important\"><span style=\"color:#0000ff !important\"><span style=\"background-color:transparent !important\"><span style=\"font-family:Arial,Helvetica,sans-serif !important\">Bigger</span></span></span></span>&nbsp;and Better.</strong><br />\r\n<span style=\"font-size:12px !important\"><span style=\"color:#0000ff !important\"><span style=\"background-color:transparent !important\"><span style=\"font-family:Arial,Helvetica,sans-serif !important\">Change</span></span></span></span>&nbsp;is inevitable, Apple plans to reveal&nbsp;iphone&nbsp;6s Plus and being one of the most iconic products of the company, it comes with many upgrades. With rounded&nbsp;<span style=\"font-size:12px !important\"><span style=\"color:#0000ff !important\"><span style=\"background-color:transparent !important\"><span style=\"font-family:Arial,Helvetica,sans-serif !important\">edges</span></span></span></span>, Apple has created a masterpiece, there is no&nbsp;<span style=\"font-size:12px !important\"><span style=\"color:#0000ff !important\"><span style=\"background-color:transparent !important\"><span style=\"font-family:Arial,Helvetica,sans-serif !important\">doubt</span></span></span></span>, wrapped in a piece of&nbsp;<span style=\"font-size:12px !important\"><span style=\"color:#0000ff !important\"><span style=\"background-color:transparent !important\"><span style=\"font-family:Arial,Helvetica,sans-serif !important\">aluminum</span></span></span></span>&nbsp;the&nbsp;<span style=\"font-size:12px !important\"><span style=\"color:#0000ff !important\"><span style=\"background-color:transparent !important\"><span style=\"font-family:Arial,Helvetica,sans-serif !important\">all new</span></span></span></span>&nbsp;6s ensures a comfort fit. Apple\'s products are always identical in design and only differs in&nbsp;<span style=\"font-size:12px !important\"><span style=\"color:#0000ff !important\"><span style=\"background-color:transparent !important\"><span style=\"font-family:Arial,Helvetica,sans-serif !important\">either</span></span></span></span>&nbsp;size of iphones with increased&nbsp;<span style=\"font-size:12px !important\"><span style=\"color:#0000ff !important\"><span style=\"background-color:transparent !important\"><span style=\"font-family:Arial,Helvetica,sans-serif !important\">length</span></span></span></span>&nbsp;&amp; call them Plus or&nbsp;<span style=\"font-size:12px !important\"><span style=\"color:#0000ff !important\"><span style=\"background-color:transparent !important\"><span style=\"font-family:Arial,Helvetica,sans-serif !important\">placement</span></span></span></span>&nbsp;of iron pieces around the borders and bezels. Apple\'s new smart devices could be a real eye-opener for who want a real smartphone, as 6s plus is the phone of the future. A stylish plate of&nbsp;aluminum&nbsp;and glass could\'ve been better, if the design department had visualized 6s Plus in a better way. Equipped with a conventional liquid crystal displays, according to a new leak, the display sizes will remain the same with 5.5 inches. Apple\'s new Force Touch Screen and pink body will be a plus for your selfie sessions. It is possible to add new colors to&nbsp;iphone, in addition to the available colors, it is assumed that Apple\'s interest is in considering the release of a pink variation along with space grey, sliver and gold.&nbsp;iPhone&nbsp;6s Plus production have already started but it should be taken into account though, that it will take some time to get released. 6s plus will take longer to reach here as Apple is orphan in Pakistan. like we all know that there is no authorized dealer of the company. 6s Plus in local market would cost you much higher than it\'s actual cost as that always happens here. especially to the&nbsp;iphonelovers. Strong and powerful novelty, but with larger screen. Well it has its own charisma if one can manage such a size from Apple. Rumored to be loaded with a sapphire display the phablet is sure to make some waves in the industry.&nbsp;&nbsp;BuildOSiOS 9 &nbsp;Dimensions158.1 x 77.8 x 7.1 mm &nbsp;Weight192 g &nbsp;SIMNano-SIM &nbsp;ColorsSpace Gray, Silver, Gold, Rose Gold&nbsp;&nbsp;Frequency2G BandGSM 850 / 900 / 1800 / 1900 &nbsp;3G BandHSDPA 850 / 900 / 1700 / 1900 / 2100 &nbsp;4G BandLTE band 1(2100), 2(1900), 3(1800), 4(1700/2100), 5(850), 7(2600), 8(900), 12(700), 13(700), 17(700), 18(800), 19(800), 20(800), 25(1900), 26(850), 28(700), 29(700), 30(2300) - A1633 &nbsp;ProcessorCPU<strong>2 GHz Dual-Core</strong>&nbsp;&nbsp;ChipsetApple A9 &nbsp;DisplayTechnologyLED-backlit IPS LCD, capacitive touchscreen, 16M colors, multitouch &nbsp;Size5.5 inches &nbsp;Resolution1080 x 1920 pixels (~401 ppi pixel density) &nbsp;ProtectionIon-Strengthened Glass, Sapphire Crystal Glass with Oleophobic Coating &nbsp;Extra Features3D Touch display, Display Zoom &nbsp;MemoryBuilt-in<strong>16/64/128GB</strong>&nbsp;built-in &nbsp;CardNo &nbsp;CameraMain12 MP, 4608 x 2592 pixels, autofocus, dual-LED dual tone flash &nbsp;FeaturesOptical image stabilization, phase detection, Geo-tagging, simultaneous HD video and image recording, touch focus, face/smile detection, HDR (photo/panorama), Video 1080p@60fps, 720p@240fps, optical stabilization &nbsp;Front5.0 MP Camera &nbsp;ConnectivityWLANWi-Fi 802.11 a/b/g/n/ac, dual-band, hotspot &nbsp;Bluetoothv4.1 with A2DP, LE &nbsp;GPSYes A-GPS support &amp; Glonass &nbsp;RadioNo &nbsp;USBUSB (v2.0, reversible connector) &nbsp;NFCYes &nbsp;Data<span style=\"color:#000077\"><strong>GPRS</strong></span>,&nbsp;<span style=\"color:#000077\"><strong>EDGE</strong></span>, 3G (HSPA 42.2/5.76 Mbps, 4G LTE Cat4 150/50 Mbps, EV-DO Rev.A 3.1 Mbps &nbsp;FeaturesSensorsAccelerometer, gyro, proximity, compass, barometer, Fingerprint &nbsp;Audio3.5mm audio jack, Speakerphone &nbsp;BrowserHTML5 (Safari) &nbsp;MessagingiMessage, SMS (threaded view), MMS, Email, Push Email &nbsp;GamesBuilt-in downloadable &nbsp;TorchYes &nbsp;ExtraActive noise cancellation with dedicated mic, Document editor, Siri natural language commands and dictation, TV-out, Photo/video editor, Apple Pay (Visa, MasterCard, AMEX certified), Voice memo/command/dial, Audio/video player and editor, &nbsp;TV-out, iCloud cloud service &nbsp;BatteryCapacity<strong>2915 mAh</strong>&nbsp;&nbsp;Standbyup to 384 hrs &nbsp;Talktimeup to 24 hrs &nbsp;Musicplayup to 80 hrs &nbsp;</p>\r\n\r\n<h2 style=\"margin-left:0px; margin-right:0px\"><span style=\"font-size:10pt\"><span style=\"font-family:Arial,Helvetica,sans-serif\"><span style=\"color:#666666\"><strong>Price</strong></span></span></span></h2>\r\n\r\n<p>Price in Rs:&nbsp;<strong>60,500</strong>&nbsp;&nbsp;&nbsp;&nbsp;Price in USD:&nbsp;<strong>$580</strong>&nbsp;&nbsp;RatingsAverage Rating is&nbsp;<strong>4.1 stars</strong>&nbsp;- based on&nbsp;<strong>245</strong>&nbsp;user reviews. &nbsp;</p>\r\n','',60500,61500,'1','1',12,'2018-02-06 20:22:00');
/*!40000 ALTER TABLE `products` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `slider`
--

DROP TABLE IF EXISTS `slider`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `slider` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(45) DEFAULT NULL,
  `image` varchar(45) DEFAULT NULL,
  `description` varchar(45) DEFAULT NULL,
  `link` varchar(255) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `slider`
--

LOCK TABLES `slider` WRITE;
/*!40000 ALTER TABLE `slider` DISABLE KEYS */;
INSERT INTO `slider` VALUES (3,'Slider 1','1517950299.jpg','','','2018-02-06 20:51:39'),(4,'Slider 2','1517950312.jpg','','','2018-02-06 20:51:52'),(5,'Slider 3','1517950323.jpg','','','2018-02-06 20:52:03');
/*!40000 ALTER TABLE `slider` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_meta`
--

DROP TABLE IF EXISTS `user_meta`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_meta` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `meta_key` varchar(55) DEFAULT NULL,
  `meta_description` text,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `FK_user_meta` (`user_id`),
  CONSTRAINT `FK_user_meta` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_meta`
--

LOCK TABLES `user_meta` WRITE;
/*!40000 ALTER TABLE `user_meta` DISABLE KEYS */;
INSERT INTO `user_meta` VALUES (2,1,'_mobile','0324-2744356','2017-11-03 20:33:22'),(3,2,'_mobile','0332-2787244','2018-02-04 12:16:43'),(4,2,'_address','test addresss','2018-02-04 12:16:43'),(5,10,'_mobile','3242744355','2018-02-08 20:24:09'),(6,10,'_address','this testing address','2018-02-08 20:24:09');
/*!40000 ALTER TABLE `user_meta` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(55) DEFAULT NULL,
  `last_name` varchar(55) DEFAULT NULL,
  `email` varchar(55) DEFAULT NULL,
  `password` varchar(55) DEFAULT NULL,
  `role` enum('user','admin','vendor') NOT NULL DEFAULT 'user',
  `status` enum('0','1') DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'raza','nasir','raza.nasir856@gmail.com','e6e061838856bf47e1de730719fb2609','admin','1','2017-11-03 20:33:22'),(2,'raza','nasir','razanasir856@yahoo.com','e6e061838856bf47e1de730719fb2609','vendor','1','2017-11-03 20:33:22'),(10,'raza','nasir','raza.nasir856@gmail.com','25d55ad283aa400af464c76d713c07ad','user','1','2018-02-07 20:40:07');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wishlist`
--

DROP TABLE IF EXISTS `wishlist`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wishlist` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `product_id` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `FK_wishlist` (`user_id`),
  KEY `FK_wishlist_product_id` (`product_id`),
  CONSTRAINT `FK_wishlist` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE,
  CONSTRAINT `FK_wishlist_product_id` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wishlist`
--

LOCK TABLES `wishlist` WRITE;
/*!40000 ALTER TABLE `wishlist` DISABLE KEYS */;
/*!40000 ALTER TABLE `wishlist` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-02-09  3:15:53

<?php
//error reporting on
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

require_once "constants.php";

// Start a Session
if (!session_id()) @session_start();

/**
 * PDO Library link = https://packagist.org/packages/slim/pdo
 */

require_once 'vendor/autoload.php';

$dsn = "mysql:host=". HOST .";dbname=". DB_NAME .";charset=utf8";
$usr = USER;
$pwd = PASSWORD;

$db = new \Slim\PDO\Database($dsn, $usr, $pwd);

/**
 * php session library flash : https://github.com/plasticbrain/PhpFlashMessages
 */

require_once 'vendor/plasticbrain/php-flash-messages/src/FlashMessages.php';
// Instantiate the class
$msg = new \Plasticbrain\FlashMessages\FlashMessages();

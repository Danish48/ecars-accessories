<?php

require_once "db.php";

// logout
if(isset($_GET['logout'])){
    if (isset($_SERVER['HTTP_COOKIE'])) { // remove all cookie
        $cookies = explode(';', $_SERVER['HTTP_COOKIE']);
        foreach($cookies as $cookie) {
            $parts = explode('=', $cookie);
            $name = trim($parts[0]);
            setcookie($name, '', time()-1000); // remove cookie
            setcookie($name, '', time()-1000, '/'); // remove cookie
        }
    }
    session_destroy();
    header("Location:login.php");
    die;
}

// get loggedIn user
if (isset($_SERVER['HTTP_COOKIE'])) {
    if(isset($_COOKIE['current_user'])){
        $user_id = $_COOKIE['current_user'];
        $getUserStatement   = $db->select()->from('users')->where('id','=',$user_id);
        $userStatement      = $getUserStatement->execute();
        $getUser            = $userStatement->fetch();
    }
}

//account setting
if(isset($_POST['account_setting'])){
    $first_name  = $_POST['first_name'];
    $last_name   = $_POST['last_name'];
    $email       = $_POST['email'];
    $user_meta   = $_POST['user_meta'];
    if($first_name == '' || $last_name == '' || $email == '' || $user_meta == ''){
        $msg->error('Please fill in all require fields');
        header("Location:account-setting.php");
        die();
    }else{
        $user_data = [
            'first_name' => $first_name,
            'last_name'  => $last_name,
            'email'      => $email
        ];
        $updateStatement = $db->update($user_data)
                                ->table('users')
                                ->where('id', '=', $getUser['id']);
        $affectedRows = $updateStatement->execute();
        //update meta_value
        foreach($user_meta as $key => $value){
            //check meta value
            $checkUserMeta = $db->select()->from('user_meta')->where('user_id','=',$user_id)->where('meta_key','=','_' . $key);
            $checkUserMeta = $checkUserMeta->execute();
            $checkUserMeta = $checkUserMeta->fetch();
            //insert
            if(empty($checkUserMeta)){
                $insertStatement = $db->insert(array('user_id', 'meta_key', 'meta_description'))
                                    ->into('user_meta')
                                    ->values(array($getUser['id'], '_' . $key, $value));
                $insertId = $insertStatement->execute(false);
            }else{
                //update
                $updateStatement = $db->update(array('meta_description' => $value))
                    ->table('user_meta')
                    ->where('user_id', '=', $getUser['id'])->where('meta_key','=','_' . $key);
                $affectedRows = $updateStatement->execute();
            }
        }
        $msg->success("Account setting has been updated successfully.");
        header("Location:account-setting.php");
        die();
    }
}

//change password
if(isset($_POST['password_reset'])){
    $new_password     = $_POST['new_password'];
    $confirm_password = $_POST['confirm_password'];
    if($new_password == '' || $confirm_password == ''){
        $msg->error('Please fill in all require fields');
        header("Location:account-setting.php");
        die();
    }else if($new_password != $confirm_password){
        $msg->error('New password and confirm password does not match');
        header("Location:account-setting.php");
        die();
    }else{
        $updateStatement = $db->update(array('password' => md5($new_password)))
                            ->table('users')
                            ->where('id', '=', $getUser['id']);
        $affectedRows = $updateStatement->execute();
        $msg->success("Password has been updated successfully.");
        header("Location:account-setting.php");
        die();
    }
}

//add product
if(isset($_POST['add_product'])){
    $brand_id          = $_POST['brand_id'];
    $category_id       = $_POST['category_id'];
    $title             = $_POST['title'];
    $short_description = $_POST['short_description'];
    $description       = urlencode($_POST['description']);
    $regular_price     = $_POST['regular_price'];
    $sale_price        = $_POST['sale_price'];
    $stock_status      = $_POST['stock_status'];
    $status            = $_POST['status'];
    $productImage      = $_FILES['file'];
    $productGalleries  = $_FILES['gallery'];

    if($brand_id == '' || $category_id == '' || $title == '' ||
        $description == '' || $regular_price == '' || $sale_price == '' || $stock_status == '' ||
        $status == '' || empty($productImage['name'])){
        $msg->error('Please fill in all require fields');
        header("Location:add-product.php");
        die();
    }else{
        $productImage_size  = $_FILES['file']['size'];
        $productImage_tmp   = $_FILES['file']['tmp_name'];
        $productImage_type  = $_FILES['file']['type'];
        $productImage_ext   = strtolower(end(explode('.',$productImage['name'])));
        $extensions = array("jpeg","jpg","png");

        if(in_array($productImage_ext,$extensions)=== false){
            $msg->error("extension not allowed, please choose a JPEG or PNG file.");
            header("Location:add-product.php");
            die();
        }
        if($productImage_size > 2097152){
            $msg->error("File size must be excately 2 MB.");
            header("Location:add-product.php");
            die();
        }
        $new_file_name = time() . '.' . $productImage_ext;
        move_uploaded_file($productImage_tmp,"../_uploads/".$new_file_name);
        // insert product
        $insertStatement = $db->insert(array('user_id', 'title', 'slug','image','description','short_description','sale_price','regular_price','status','stock_status','brand_id'))
                            ->into('products')
                            ->values(array($getUser['id'], $title, create_slug($title),$new_file_name,$description,$short_description,$sale_price,$regular_price,$status,$stock_status,$brand_id ));
        $insertId = $insertStatement->execute(true);
        //insert product category
        $insertCategoryStatement = $db->insert(array('product_id','category_id'))->into('product_category')->values(array($insertId,$category_id));
        $insertCategoryStatement->execute(false);

        //product gallery
        if(count($productGalleries['name'])){
            for($i=0; $i < count($productGalleries['name']); $i++){

                $product_gallery_ext   = strtolower(end(explode('.',$productGalleries['name'][$i])));
                if(in_array($product_gallery_ext,$extensions) === true && $productGalleries['size'][$i] < 2097153){
                    $new_file_name = $i . time() . '.' . $product_gallery_ext;
                    move_uploaded_file($productGalleries['tmp_name'][$i],"../_uploads/".$new_file_name);
                    //insert product gallery
                    $insertGalleryStatement = $db->insert(array('product_id','image'))->into('product_gallery')->values(array($insertId,$new_file_name));
                    $insertGalleryStatement->execute(false);
                }
            }
        }
        $msg->success("Product has been added successfully.");
        header("Location:products.php");
        die();
    }
}

//update product
if(isset($_POST['update_product'])){
    $brand_id          = $_POST['brand_id'];
    $category_id       = $_POST['category_id'];
    $title             = $_POST['title'];
    $short_description = $_POST['short_description'];
    $description       = $_POST['description'];
    $regular_price     = $_POST['regular_price'];
    $sale_price        = $_POST['sale_price'];
    $stock_status      = $_POST['stock_status'];
    $status            = $_POST['status'];
    $productImage      = $_FILES['file'];
    $productGalleries  = $_FILES['gallery'];
    $product_id        = $_POST['id'];
    if($brand_id == '' || $category_id == '' || $title == '' ||
        $description == '' || $regular_price == '' || $sale_price == '' || $stock_status == '' ||
        $status == '' ){
        $msg->error('Please fill in all require fields');
        header("Location:edit-product.php?id=" . $product_id);
        die();
    }else{
        if(!empty($productImage['name'])){
            $productImage_size  = $_FILES['file']['size'];
            $productImage_tmp   = $_FILES['file']['tmp_name'];
            $productImage_type  = $_FILES['file']['type'];
            $productImage_ext   = strtolower(end(explode('.',$productImage['name'])));
            $extensions = array("jpeg","jpg","png");

            if(in_array($productImage_ext,$extensions)=== false){
                $msg->error("extension not allowed, please choose a JPEG or PNG file.");
                header("Location:add-product.php");
                die();
            }
            if($productImage_size > 2097152){
                $msg->error("File size must be excately 2 MB.");
                header("Location:add-product.php");
                die();
            }
            $new_file_name = time() . '.' . $productImage_ext;
            move_uploaded_file($productImage_tmp,"../_uploads/".$new_file_name);
        }else{
            $new_file_name = $_POST['old_file'];
        }
        // update product
        $product_data = [
            'title'             => $title, 
            'slug'              => create_slug($title),
            'image'             => $new_file_name,
            'description'       => $description,
            'short_description' => $short_description,
            'sale_price'        => $sale_price,
            'regular_price'     => $regular_price,
            'status'            => $status,
            'stock_status'      => $stock_status,
            'brand_id'          => $brand_id
        ];
        $updateStatement = $db->update($product_data)
                                ->table('products')
                                ->where('id', '=', $product_id);
        $affectedRows = $updateStatement->execute();

        
        //update product category
        $updateCategoryStatement = $db->update(array('category_id' => $category_id))->table('product_category')->where('product_id','=',$product_id);
        $updateCategoryStatement->execute();

        //product gallery
        if(count($productGalleries['name'])){
            for($i=0; $i < count($productGalleries['name']); $i++){
                $product_gallery_ext   = strtolower(end(explode('.',$productGalleries['name'][$i])));
                if(in_array($product_gallery_ext,$extensions) === true && $productGalleries['size'][$i] < 2097153){
                    $new_file_name = $i . time() . '.' . $product_gallery_ext;
                    move_uploaded_file($productGalleries['tmp_name'][$i],"../_uploads/".$new_file_name);
                    //insert product gallery
                    $insertGalleryStatement = $db->insert(array('product_id','image'))->into('product_gallery')->values(array($product_id,$new_file_name));
                    $insertGalleryStatement->execute(false);
                }
            }
        }
        $msg->success("Product has been updated successfully.");
        header("Location:products.php");
        die();
    }
}

//delete record
if(isset($_GET['delete_record'])){
    $id       = $_GET['id'];
    $table    = $_GET['table'];
    $location = $_GET['location'];
    $deleteStatement = $db->delete()->from($table)->where('id', '=', $id);
    $affectedRows = $deleteStatement->execute();
    $msg->success("Record has been deleted successfully.");
    header("Location:" . $location);
    die();
}

function create_slug($string){
    $slug=preg_replace('/[^A-Za-z0-9-]+/', '-', $string);
    return $slug;
}

function getUserMetaValue($db,$user_id,$meta_key)
{
    $getUserMeta   = $db->select()->from('user_meta')->where('user_id','=',$user_id)->where('meta_key','=',$meta_key);
    $getUserMeta   = $getUserMeta->execute();
    $getUserMeta   = $getUserMeta->fetch();
    if(count($getUserMeta)){
        return $getUserMeta['meta_description'];
    }else{
        return false;
    }
}
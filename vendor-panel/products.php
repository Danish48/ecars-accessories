<?php
    require_once "header.php";
    $sql = "SELECT p.*, b.title AS brand_title, u.first_name, u.last_name FROM products p 
            INNER JOIN brands b ON p.brand_id = b.id
            INNER JOIN users u ON u.id = p.user_id
            WHERE p.user_id = {$getUser['id']} ";
    $getProducts = $db->query($sql)->fetchAll(PDO::FETCH_ASSOC)
?>
<div id="page-wrapper">
    <div class="main-page">
        <div class="row">
            <h1>Products</h1>
        </div>
        <div class="row">
            <?php require_once "flash_message.php" ?>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        Product Listing
                        <a href="add-product.php" class="btn btn-info pull-right" style="margin-top: -10px;">Add Product</a>
                    </div>
                    <div class="panel-body">
                        <table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Brand</th>
                                <th>Product Title</th>
                                <th>Status</th>
                                <th>Stock Status</th>
                                <th>created_at</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                                if(!empty($getProducts)) {
                                    $index = '';
                                    foreach($getProducts as $product){
                                    $index++;
                             ?>
                                    <tr>
                                        <td><?php echo $index ?></td>
                                        <td><?php echo $product['brand_title'] ?></td>
                                        <td><?php echo $product['title'] ?></td>
                                        <td><?php echo $product['status'] == '1' ? '<a class="btn btn-sm btn-success">Active</a>' : '<a class="btn btn-sm btn-danger">In-active</a>' ?></td>
                                        <td><?php echo $product['stock_status'] == '1' ? '<a class="btn btn-sm btn-success">In-Stock</a>' : '<a class="btn btn-sm btn-danger">Out-Of-Stock</a>' ?></td>
                                        <td><?php echo date('d-m-Y',strtotime($product['created_at'])); ?></td>
                                        <td>
                                            <a class="delete_record" data-href="<?php echo $_SERVER['PHP_SELF'] . '?delete_record=true&location=products.php&table=products&id=' . $product['id'] ?>"><i class="fa fa-trash-o"></i></a>
                                            <a href="edit-product.php?id=<?php echo $product['id'] ?>"><i class="fa fa-pencil-square"></i></a>
                                        </td>
                                    </tr>
                            <?php
                                    }
                                }
                            ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
require_once "footer.php";
?>

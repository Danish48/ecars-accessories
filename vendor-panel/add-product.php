<?php
require_once "header.php";
//get brands
$getBrands = $db->select()->from('brands')->where('status','=','1')->orderBy('title','ASC');
$getBrands = $getBrands->execute();
$getBrands = $getBrands->fetchAll(PDO::FETCH_ASSOC);
//get categories
$getCategories = $db->select()->from('categories')->where('status','=','1')->where('parent_id','!=',0)->orderBy('title','ASC');
$getCategories = $getCategories->execute();
$getCategories = $getCategories->fetchAll(PDO::FETCH_ASSOC);
?>
<div id="page-wrapper">
    <div class="main-page">
        <div class="row">
            <h1>Products</h1>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <?php require_once "flash_message.php" ?>
                <div class="panel panel-info">
                    <div class="panel-heading">
                        Add Product
                    </div>
                    <div class="panel-body">
                        <form method="post" action="<?php echo $_SERVER['PHP_SELF']?>" enctype="multipart/form-data">
                            <input type="hidden" name="add_product" value="1">
                            <div class="form-body">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="control-label">Brand<span class="text-danger">*</span></label>
                                            <select name="brand_id" id="dropdown" class="form-control">
                                                <option value=""> -- Select Brand -- </option>
                                                <?php
                                                    if(!empty($getBrands)){
                                                        foreach ($getBrands as $brands){
                                                            echo '<option value="'. $brands['id'] .'">'. $brands['title'] .'</option>';
                                                        }
                                                    }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                    <!--/span-->
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="control-label">Categories<span class="text-danger">*</span></label>
                                            <select name="category_id" class="form-control">
                                                <option value=""> -- Select Categories -- </option>
                                                <?php
                                                if(!empty($getCategories)){
                                                    foreach ($getCategories as $category){
                                                        echo '<option value="'. $category['id'] .'">'. $category['title'] .'</option>';
                                                    }
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                    <!--/span-->
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="control-label">Title<span class="text-danger">*</span></label>
                                            <input type="text" name="title" class="form-control">
                                        </div>
                                    </div>
                                    <!--/span-->
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="control-label">Feature Image<span class="text-danger">*</span></label>
                                            <input type="file" name="file" class="form-control">
                                        </div>
                                    </div>
                                    <!--/span-->
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="control-label">Short Description<span class="text-danger"></span></label>
                                            <textarea name="short_description" class="form-control"></textarea>
                                        </div>
                                    </div>
                                    <!--/span-->
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="control-label">Description<span class="text-danger">*</span></label>
                                            <textarea id="editor1" name="description" class="form-control"></textarea>
                                        </div>
                                    </div>
                                    <!--/span-->
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="control-label">Regualr Price<span class="text-danger">*</span></label>
                                            <input type="text" name="regular_price" class="form-control">
                                        </div>
                                    </div>
                                    <!--/span-->
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="control-label">Sale Price<span class="text-danger"></span></label>
                                            <input type="text" name="sale_price" class="form-control">
                                        </div>
                                    </div>
                                    <!--/span-->
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="control-label">Stock Status<span class="text-danger">*</span></label>
                                            <select name="stock_status" class="form-control">
                                                <option value="1">In-Stock</option>
                                                <option value="0">Out-Of-Stock</option>
                                            </select>
                                        </div>
                                    </div>
                                    <!--/span-->
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="control-label">Status<span class="text-danger">*</span></label>
                                            <select name="status" class="form-control">
                                                <option value="1">Active</option>
                                                <option value="0">In-Active</option>
                                            </select>
                                        </div>
                                    </div>
                                    <!--/span-->
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="control-label">Product Gallery<span class="text-danger"></span></label>
                                            <input type="file" class="form-control" name="gallery[]" multiple>
                                        </div>
                                    </div>
                                    <!--/span-->
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                         <button type="submit" class="btn btn-success"> <i class="fa fa-check"></i> Save</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function(){
        CKEDITOR.replace( 'editor1' );
    })
</script>
<?php
require_once "footer.php";
?>

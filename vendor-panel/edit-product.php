<?php
require_once "header.php";
//get brands
$getBrands = $db->select()->from('brands')->where('status','=','1')->orderBy('title','ASC');
$getBrands = $getBrands->execute();
$getBrands = $getBrands->fetchAll(PDO::FETCH_ASSOC);
//get categories
$getCategories = $db->select()->from('categories')->where('status','=','1')->where('parent_id','!=',0)->orderBy('title','ASC');
$getCategories = $getCategories->execute();
$getCategories = $getCategories->fetchAll(PDO::FETCH_ASSOC);
//get product
$getProduct = $db->select()->from('products')->where('id','=',$_GET['id']);
$getProduct = $getProduct->execute();
$getProduct = $getProduct->fetch();
//get product categories
$getProductCat = $db->select()->from('product_category')->where('product_id','=',$_GET['id']);
$getProductCat = $getProductCat->execute();
$getProductCat = $getProductCat->fetch();
//get product galleries
$getProductGalleries = $db->select()->from('product_gallery')->where('product_id','=',$_GET['id']);
$getProductGalleries = $getProductGalleries->execute();
$getProductGalleries = $getProductGalleries->fetchAll(PDO::FETCH_ASSOC);
?>
<div id="page-wrapper">
    <div class="main-page">
        <div class="row">
            <h1>Products</h1>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <?php require_once "flash_message.php" ?>
                <div class="panel panel-info">
                    <div class="panel-heading">
                        Edit Product
                    </div>
                    <div class="panel-body">
                        <form method="post" action="<?php echo $_SERVER['PHP_SELF']?>" enctype="multipart/form-data">
                            <input type="hidden" name="update_product" value="1">
                            <input type="hidden" name="id" value="<?php echo $getProduct['id'] ?>">
                            <div class="form-body">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="control-label">Brand<span class="text-danger">*</span></label>
                                            <select name="brand_id" id="dropdown" class="form-control">
                                                <option value=""> -- Select Brand -- </option>
                                                <?php
                                                    if(!empty($getBrands)){
                                                        foreach ($getBrands as $brands){
                                                            $selected = $getProduct['brand_id'] == $brands['id'] ? "selected" : '';
                                                            echo '<option value="'. $brands['id'] .'" '. $selected .'>'. $brands['title'] .'</option>';
                                                        }
                                                    }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                    <!--/span-->
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="control-label">Categories<span class="text-danger">*</span></label>
                                            <select name="category_id" class="form-control">
                                                <option value=""> -- Select Categories -- </option>
                                                <?php
                                                if(!empty($getCategories)){
                                                    foreach ($getCategories as $category){
                                                        $selected = $getProductCat['category_id'] == $category['id'] ? 'selected' : '';
                                                        echo '<option value="'. $category['id'] .'" '. $selected .'>'. $category['title'] .'</option>';
                                                    }
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                    <!--/span-->
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="control-label">Title<span class="text-danger">*</span></label>
                                            <input type="text" name="title" class="form-control" value="<?php echo $getProduct['title'] ?>">
                                        </div>
                                    </div>
                                    <!--/span-->
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="control-label">Feature Image<span class="text-danger">*</span></label>
                                            <input type="file" name="file" class="form-control">
                                            <input type="hidden" name="old_file" value="<?php echo $getProduct['image'] ?>">
                                        </div>
                                        <img style="width:150px;object-fit:contain;" src="../_uploads/<?php echo $getProduct['image'] ?>">
                                    </div>
                                    <!--/span-->
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="control-label">Short Description<span class="text-danger">*</span></label>
                                            <textarea name="short_description" class="form-control"><?php echo $getProduct['short_description'] ?></textarea>
                                        </div>
                                    </div>
                                    <!--/span-->
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="control-label">Description<span class="text-danger">*</span></label>
                                            <textarea id="editor1" name="description" class="form-control"><?php echo urldecode($getProduct['description']) ?></textarea>
                                        </div>
                                    </div>
                                    <!--/span-->
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="control-label">Regualr Price<span class="text-danger">*</span></label>
                                            <input type="text" name="regular_price" class="form-control" value="<?php echo $getProduct['regular_price'] ?>">
                                        </div>
                                    </div>
                                    <!--/span-->
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="control-label">Sale Price<span class="text-danger"></span></label>
                                            <input type="text" name="sale_price" class="form-control" value="<?php echo $getProduct['sale_price'] ?>">
                                        </div>
                                    </div>
                                    <!--/span-->
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="control-label">Stock Status<span class="text-danger">*</span></label>
                                            <select name="stock_status" class="form-control">
                                                <option value="1" <?php echo $getProduct['stock_status'] == '1' ? 'selected' : '' ?> >In-Stock</option>
                                                <option value="0" <?php echo $getProduct['stock_status'] == '0' ? 'selected' : '' ?> >Out-Of-Stock</option>
                                            </select>
                                        </div>
                                    </div>
                                    <!--/span-->
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="control-label">Status<span class="text-danger">*</span></label>
                                            <select name="status" class="form-control">
                                                <option value="1" <?php echo $getProduct['status'] == '1' ? 'selected' : '' ?>>Active</option>
                                                <option value="0" <?php echo $getProduct['status'] == '1' ? 'selected' : '' ?>>In-Active</option>
                                            </select>
                                        </div>
                                    </div>
                                    <!--/span-->
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="control-label">Product Gallery<span class="text-danger"></span></label>
                                            <input type="file" class="form-control" name="gallery[]" multiple>
                                        </div>
                                    </div>
                                    <!--/span-->
                                    <?php
                                        if(!empty($getProductGalleries)) {
                                            foreach ($getProductGalleries as $gallery) {
                                    ?>
                                                <div class="col-lg-2" style="position: relative">
                                                    <img style="width: 150px;object-fit:contain" src="../_uploads/<?php echo $gallery['image'] ?>">
                                                    <a class="delete_record" data-href="<?php echo $_SERVER['PHP_SELF'] . '?delete_record=true&location=edit-product.php?id='. $getProduct['id'] .'&table=product_gallery&id=' . $gallery['id'] ?>" style="position:absolute;top:0;left:10px;" title="Delete"><i class="fa fa-close"></i></a>
                                                </div>
                                    <?php
                                            }
                                        }
                                    ?>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                         <button type="submit" class="btn btn-success"> <i class="fa fa-check"></i> Save</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function(){
        CKEDITOR.replace( 'editor1' );
    })
</script>
<?php
require_once "footer.php";
?>

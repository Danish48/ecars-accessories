<?php
    require_once "header.php";
    $sql = "SELECT * FROM `order` o
            INNER JOIN users u ON u.id = o.user_id";
    $getOrders = $db->query($sql)->fetchAll(PDO::FETCH_ASSOC)
?>
<div id="page-wrapper">
    <div class="main-page">
        <div class="row">
            <h1>Products</h1>
        </div>
        <div class="row">
            <?php require_once "flash_message.php" ?>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        Product Listing
                    </div>
                    <div class="panel-body">
                        <table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">
                            <thead>
                            <tr>
                                <th>Order ID</th>
                                <th>Customer Name</th>
                                <th>Email</th>
                                <th>Mobile No</th>
                                <th>Address</th>
                                <th>Order Price</th>
                                <th>Status</th>
                                <th>created_at</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                                if(!empty($getOrders)) {
                                    $index = '';
                                    foreach($getOrders as $order){
                                    $index++;
                             ?>
                                    <tr>
                                        <td><?php echo $index ?></td>
                                        <td><?php echo $order['fullname'] ?></td>
                                        <td><?php echo $order['email'] ?></td>
                                        <td><?php echo $order['mobile_no'] ?></td>
                                        <td><?php echo $order['billing_address'] ?></td>
                                        <td><?php echo $order['order_price'] ?></td>
                                        <td><?php echo $order['status'] ?></td>
                                        <td><?php echo date('d-m-Y',strtotime($order['created_at'])); ?></td>
                                        <td><a class="delete_record" data-href="<?php echo $_SERVER['PHP_SELF'] . '?delete_record=true&location=orders.php&table=order&id=' . $order['id'] ?>"><i class="fa fa-trash-o"></i></a></td>
                                    </tr>
                            <?php
                                    }
                                }
                            ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
require_once "footer.php";
?>

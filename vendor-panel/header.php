<?php
require_once "action.php";
if(!isset($_COOKIE['current_user'])){
    header("Location:login.php");
    die();
}
if($getUser['role'] != 'vendor'){
    header("Location:../index.php");
    die();
}
$current_url = explode('/',$_SERVER['HTTP_HOST'] . $_SERVER['PHP_SELF']);
?>
<!DOCTYPE HTML>
<html>
<head>
    <title><?php echo str_replace('-',' ',end($current_url)) ?> | Admin Panel</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="keywords" content="Glance Design Dashboard Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template,
SmartPhone Compatible web template, free WebDesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
    <link href="assets/css/bootstrap.css" rel='stylesheet' type='text/css' />
    <link href="assets/css/style.css" rel='stylesheet' type='text/css' />
    <link href="assets/css/font-awesome.css" rel="stylesheet">
    <link href='assets/css/SidebarNav.min.css' media='all' rel='stylesheet' type='text/css'/>
    <link href="//fonts.googleapis.com/css?family=PT+Sans:400,400i,700,700i&amp;subset=cyrillic,cyrillic-ext,latin-ext" rel="stylesheet">
    <link href="assets/css/custom.css" rel="stylesheet">
    <link href="assets/css/dataTables.bootstrap.min.css" rel="stylesheet" type="text/css">
    <script src="assets/js/jquery-1.11.1.min.js"></script>
    <script src="assets/js/modernizr.custom.js"></script>
    <script src="assets/js/classie.js"></script>
    <script src="assets/js/bootstrap.js"></script>
    <script src="assets/js/jquery.dataTables.min.js"></script>
    <script src="assets/js/dataTables.bootstrap.min.js"></script>
    <script src="assets/ckeditor/ckeditor.js"></script>
    <script src="assets/js/custom.js"></script>
</head>
<body class="cbp-spmenu-push">
<div class="main-content">
    <div class="cbp-spmenu cbp-spmenu-vertical cbp-spmenu-left" id="cbp-spmenu-s1">
        <!--left-fixed -navigation-->
        <aside class="sidebar-left">
            <nav class="navbar navbar-inverse">
                <div class="navbar-header">
                    <h1><a class="navbar-brand" href="<?php echo $_SERVER['PHP_SELF'] ?>"><span class="fa fa-area-chart"></span> Glance<span class="dashboard_text">Design dashboard</span></a></h1>
                </div>
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="sidebar-menu">
                        <li class="header">MAIN NAVIGATION</li>
                        <li class="treeview">
                            <a href="dashboard.php">
                                <i class="fa fa-dashboard"></i> <span>Dashboard</span>
                            </a>
                        </li>
                        <li class="treeview">
                            <a href="products.php">
                                <i class="fa fa-product-hunt"></i> <span>Products</span>
                            </a>
                        </li>
                        <li class="treeview">
                            <a href="orders.php">
                                <i class="fa fa-shopping-bag"></i> <span>Orders</span>
                            </a>
                        </li>
                    </ul>
                </div>
                <!-- /.navbar-collapse -->
            </nav>
        </aside>
    </div>
    <div class="sticky-header header-section ">
        <div class="header-right">
            <div class="profile_details">
                <ul>
                    <li class="dropdown profile_details_drop">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                            <div class="profile_img">
                                <div class="user-name">
                                    <p><?php echo $getUser['first_name'] . ' ' . $getUser['last_name'] ?></p>
                                    <span>Administrator</span>
                                </div>
                                <i class="fa fa-angle-down lnr"></i>
                                <i class="fa fa-angle-up lnr"></i>
                                <div class="clearfix"></div>
                            </div>
                        </a>
                        <ul class="dropdown-menu drp-mnu">
                            <li> <a href="account-setting.php"><i class="fa fa-user"></i> My Account</a> </li>
                            <li> <a href="<?php echo $_SERVER['PHP_SELF'] . '?logout=true' ?>"><i class="fa fa-sign-out"></i> Logout</a> </li>
                        </ul>
                    </li>
                </ul>
            </div>
            <div class="clearfix"> </div>
        </div>
        <div class="clearfix"> </div>
    </div>
    <!-- //header-ends -->
<?php
  if(isset($_COOKIE['current_user'])){
    header("Location:index.php");
    die();
  }
  require_once "header.php";
?>
  <div id="container">
    <div class="container">
      <div class="row">
        <!--Middle Part Start-->
        <?php require_once "flash_message.php"; ?>
        <form method="post">
          <input type="hidden" name="login" value="1"> 
          <div id="content" class="col-sm-9">
          <h1 class="title">Login</h1>
          <div class="row">
            <div class="col-sm-6">
                <div class="form-group">
                  <label class="control-label" for="input-email">E-Mail Address</label>
                  <input type="text" name="email" value="" placeholder="E-Mail Address" id="input-email" class="form-control" />
                </div>
                <div class="form-group">
                  <label class="control-label" for="input-password">Password</label>
                  <input type="password" name="password" value="" placeholder="Password" id="input-password" class="form-control" />
                  <br />
                </div>
                <p><a href="forgot-password.php">Forgot Password</a> | <a href="vendor-panel/login.php">Sign in by vendor</a></p>
                <input type="submit" value="Login" class="btn btn-primary" />
            </div>
          </div>
        </div>
        </form>  
      </div>
    </div>
  </div>
<?php
  require_once "footer.php";
?>

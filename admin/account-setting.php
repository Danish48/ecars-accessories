<?php
require_once "header.php";
?>
<div id="page-wrapper">
    <div class="main-page">
        <div class="row">
            <h1>Account Setting</h1>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <?php require_once "flash_message.php" ?>
                <div class="panel panel-info">
                    <div class="panel-heading">
                        General Info
                    </div>
                    <div class="panel-body">
                        <form method="post" action="<?php echo $_SERVER['PHP_SELF']?>" enctype="multipart/form-data">
                            <input type="hidden" name="account_setting" value="1">
                            <div class="form-body">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="control-label">First Name<span class="text-danger">*</span></label>
                                            <input type="text" name="first_name" max="12" class="form-control" value="<?php echo $getUser['first_name'] ?>">
                                        </div>
                                    </div>
                                    <!--/span-->
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="control-label">Last Name<span class="text-danger">*</span></label>
                                            <input type="text" name="last_name" max="12" class="form-control" value="<?php echo $getUser['last_name'] ?>">
                                        </div>
                                    </div>
                                    <!--/span-->
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="control-label">Email<span class="text-danger">*</span></label>
                                            <input type="text" name="email" max="12" class="form-control" value="<?php echo $getUser['email'] ?>">
                                        </div>
                                    </div>
                                    <!--/span-->
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="control-label">Mobile No<span class="text-danger">*</span></label>
                                            <input type="text" name="user_meta[mobile]" max="12" class="form-control" value="<?php echo getUserMetaValue($db,$getUser['id'],'_mobile') ?>">
                                        </div>
                                    </div>
                                    <!--/span-->
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                         <button type="submit" class="btn btn-success"> <i class="fa fa-check"></i> Save</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>

                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        Reset Password
                    </div>
                    <div class="panel-body">
                        <form method="post" action="<?php echo $_SERVER['PHP_SELF']?>">
                            <input type="hidden" name="password_reset" value="1">
                            <div class="form-body">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="control-label">New Password<span class="text-danger">*</span></label>
                                            <input type="password" name="new_password" max="20" class="form-control">
                                        </div>
                                    </div>
                                    <!--/span-->
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="control-label">Confirm Password<span class="text-danger">*</span></label>
                                            <input type="password" name="confirm_password" max="20" class="form-control">
                                        </div>
                                    </div>
                                    <!--/span-->
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <button type="submit" class="btn btn-success"> <i class="fa fa-check"></i> Save</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
<?php
require_once "footer.php";
?>

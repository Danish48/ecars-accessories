<?php
    require_once "header.php";
    $getSlider   = $db->select()->from('slider');
    $getSlider   = $getSlider->execute();
    $getSlider   = $getSlider->fetchAll(PDO::FETCH_ASSOC);
?>
<div id="page-wrapper">
    <div class="main-page">
        <div class="row">
            <h1>Home Slider</h1>
        </div>
        <div class="row">
            <?php require_once "flash_message.php" ?>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        Slider Listing
                        <a href="add-slider.php" class="btn btn-info pull-right" style="margin-top: -10px;">Add Slider</a>
                    </div>
                    <div class="panel-body">
                        <table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Image</th>
                                <th>created_at</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                                if(!empty($getSlider)) {
                                    $index = '';
                                    foreach($getSlider as $slider){
                                    $index++;
                             ?>
                                    <tr>
                                        <td><?php echo $index ?></td>
                                        <td><img style="object-fit: contain;width: 150px;" src="../_uploads/<?php echo $slider['image'] ?>"></td>
                                        <td><?php echo date('d-m-Y',strtotime($slider['created_at'])); ?></td>
                                        <td><a class="delete_record" data-href="<?php echo $_SERVER['PHP_SELF'] . '?delete_record=true&location=slider.php&table=slider&id=' . $slider['id'] ?>"><i class="fa fa-trash-o"></i></a></td>
                                    </tr>
                            <?php
                                    }
                                }
                            ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
require_once "footer.php";
?>

<?php
    require_once "header.php";

    $sql = "SELECT b.*, group_concat(c.title) as category FROM brands b 
            LEFT JOIN brand_category bc ON bc.brand_id = b.id
            LEFT JOIN categories c ON c.id = bc.category_id
            group by b.id";
    $getBrands   =$db->query($sql)->fetchAll(PDO::FETCH_ASSOC)
?>
<div id="page-wrapper">
    <div class="main-page">
        <div class="row">
            <h1>Brands</h1>
        </div>
        <div class="row">
            <?php require_once "flash_message.php" ?>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        Brand Listing
                        <a href="add-brand.php" class="btn btn-info pull-right" style="margin-top: -10px;">Add Brand</a>
                    </div>
                    <div class="panel-body">
                        <table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Category</th>
                                <th>Image</th>
                                <th>Title</th>
                                <th>status</th>
                                <th>created_at</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                                if(!empty($getBrands)) {
                                    $index = '';
                                    foreach($getBrands as $brand){
                                    $index++;
                             ?>
                                    <tr>
                                        <td><?php echo $index ?></td>
                                        <td><?php echo $brand['category'] ?></td>
                                        <td><img style="object-fit: contain;width: 150px;" src="../_uploads/<?php echo $brand['image'] ?>"></td>
                                        <td><?php echo $brand['title'] ?></td>
                                        <td><?php echo $brand['status'] == '1' ? '<a class="btn btn-sm btn-success">Active</a>' : '<a class="btn btn-sm btn-danger">In-active</a>' ?></td>
                                        <td><?php echo date('d-m-Y',strtotime($brand['created_at'])); ?></td>
                                        <td><a class="delete_record" data-href="<?php echo $_SERVER['PHP_SELF'] . '?delete_record=true&location=brands.php&table=brands&id=' . $brand['id'] ?>"><i class="fa fa-trash-o"></i></a></td>
                                    </tr>
                            <?php
                                    }
                                }
                            ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
require_once "footer.php";
?>

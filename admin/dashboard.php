<?php 
	require_once "header.php";
	$dataPointsLine = array(
		array("x" => 10, "y" => 71),
		array("x" => 20, "y" => 55),
		array("x" => 30, "y" => 50),
		array("x" => 40, "y" => 65),
		array("x" => 50, "y" => 95),
		array("x" => 60, "y" => 68),
		array("x" => 70, "y" => 28),
		array("x" => 80, "y" => 34),
		array("x" => 90, "y" => 14),
		array("x" => 100, "y" => 33),
		array("x" => 110, "y" => 42),
		array("x" => 120, "y" => 62),
		array("x" => 130, "y" => 70),
		array("x" => 140, "y" => 85),
		array("x" => 150, "y" => 58),
		array("x" => 160, "y" => 34),
		array("x" => 170, "y" => 24),
		array("x" => 180, "y" => 33),
		array("x" => 190, "y" => 28),
		array("x" => 200, "y" => 42)
	);
	$dataPointsBar = array(
		array("y" => 6, "label" => "Apple"),
		array("y" => 4, "label" => "Mango"),
		array("y" => 5, "label" => "Orange"),
		array("y" => 7, "label" => "Banana"),
		array("y" => 4, "label" => "Pineapple"),
		array("y" => 6, "label" => "Pears"),
		array("y" => 7, "label" => "Grapes"),
		array("y" => 5, "label" => "Lychee"),
		array("y" => 4, "label" => "Jackfruit")
	);
?>
<script src="https://canvasjs.com/assets/script/canvasjs.min.js"></script>
<div id="page-wrapper">
		<div class="row">
			<div class="col-lg-12">
				<h1>Dashboard</h1>
			</div>
			<div class="col_3">
				<div class="col-md-3 widget widget1">
					<div class="r3_counter_box">
						<i class="pull-left fa fa-dollar icon-rounded"></i>
						<div class="stats">
							<h5><strong>$452</strong></h5>
							<span>Total Revenue</span>
						</div>
					</div>
				</div>
				<div class="col-md-3 widget widget1">
					<div class="r3_counter_box">
						<i class="pull-left fa fa-laptop user1 icon-rounded"></i>
						<div class="stats">
							<h5><strong>$1019</strong></h5>
							<span>Online Revenue</span>
						</div>
					</div>
				</div>
				<div class="col-md-3 widget widget1">
					<div class="r3_counter_box">
						<i class="pull-left fa fa-money user2 icon-rounded"></i>
						<div class="stats">
							<h5><strong>$1012</strong></h5>
							<span>Expenses</span>
						</div>
					</div>
				</div>
				<div class="col-md-3 widget widget1">
					<div class="r3_counter_box">
						<i class="pull-left fa fa-pie-chart dollar1 icon-rounded"></i>
						<div class="stats">
							<h5><strong>$450</strong></h5>
							<span>Expenditure</span>
						</div>
					</div>
				</div>
				<div class="col-md-3 widget">
					<div class="r3_counter_box">
						<i class="pull-left fa fa-users dollar2 icon-rounded"></i>
						<div class="stats">
							<h5><strong>1450</strong></h5>
							<span>Total Users</span>
						</div>
					</div>
				</div>
				<div class="clearfix"> </div>
			</div>
		</div>
		<div class="row" style="margin-top: 50px">
			<div class="col-lg-6">
				<div id="barChartContainer"></div>
			</div>
			<div class="col-lg-6">
				<div id="lineChartContainer"></div>
			</div>
		</div>
<!--		<div class="row" style="margin-top: 50px">-->
<!--			<div class="col-lg-12">-->
<!--				<div id="lineChartContainer"></div>-->
<!--			</div>-->
<!--		</div>-->
</div>
<script type="text/javascript">
	$(function () {
		//line chart
		var chart = new CanvasJS.Chart("lineChartContainer", {
			theme: "light2",
			zoomEnabled: true,
			animationEnabled: true,
			title: {
				text: "Line Chart in PHP using CanvasJS"
			},
			subtitles: [
				{
					text: "Try Zooming and Panning"
				}
			],
			data: [
				{
					type: "line",
					dataPoints: <?php echo json_encode($dataPointsLine, JSON_NUMERIC_CHECK); ?>
				}
			]
		});
		chart.render();
		//bar chart
		var chart2 = new CanvasJS.Chart("barChartContainer", {
			theme: "theme2",
			animationEnabled: true,
			title: {
				text: "Simple Column Chart in PHP"
			},
			data: [
				{
					type: "column",
					dataPoints: <?php echo json_encode($dataPointsBar, JSON_NUMERIC_CHECK); ?>
				}
			]
		});
		chart2.render();
	});
</script>
<?php
	require_once "footer.php";
?>
	
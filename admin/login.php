<?php
require_once "db.php";
//redirect if user logged in
if(isset($_COOKIE['current_user'])){
	header("Location:dashboard.php");
	die();
}
//if user submit login form
if(isset($_POST['sign_in'])){
	$email    = $_POST['email'];
	$password = $_POST['password'];
	if($email == '' || $password == ''){
		$msg->error('Email and password field is required');
		header("Location:login.php");
		die();
	}else{
		$getUserStatement   = $db->select()->from('users')->where('email','=',$email)->where('password','=',md5($password))->where('role','=','admin');
		$userStatement      = $getUserStatement->execute();
		$getUser            = $userStatement->fetch();
		if(empty($getUser)){
			$msg->error('Invalid credentials');
			header("Location:login.php");
			die();
		}else{
			setcookie("current_user", $getUser['id'], time() + (86400 * 30), "/");
			header("Location:dashboard.php");
		}
	}
}
?>
<!DOCTYPE HTML>
<html>
<head>
<title>Ecars Online Store | Admin Panel</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="Glance Design Dashboard Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
SmartPhone Compatible web template, free WebDesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
<link href="assets/css/bootstrap.css" rel='stylesheet' type='text/css' />
<link href="assets/css/style.css" rel='stylesheet' type='text/css' />
<link href="assets/css/font-awesome.css" rel="stylesheet">
<link href='assets/css/SidebarNav.min.css' media='all' rel='stylesheet' type='text/css'/>
<script src="assets/js/jquery-1.11.1.min.js"></script>
<script src="assets/js/modernizr.custom.js"></script>
<link href="//fonts.googleapis.com/css?family=PT+Sans:400,400i,700,700i&amp;subset=cyrillic,cyrillic-ext,latin-ext" rel="stylesheet">
</head>
<body class="cbp-spmenu-push">
<div class="main-content" style="background: #F1F1F1;">
		<div id="page-wrapper">
			<div class="main-page login-page ">
				<h2 class="title1">Login</h2>
				<div class="widget-shadow">
					<div class="login-body">
						<?php require_once "flash_message.php" ?>
						<form  method="post">
							<input type="email" class="user" name="email" placeholder="Enter Your Email" required="">
							<input type="password" name="password" class="lock" placeholder="Password" required="">
							<input type="submit" name="sign_in" value="Sign In">
						</form>
					</div>
				</div>
			</div>
		</div>
		<div class="footer">
		   <p>&copy; <?php echo date('Y') ?>  All Rights Reserved </p>
		</div>
	</div>
<script src='assets/js/SidebarNav.min.js' type='text/javascript'></script>
<script src="assets/js/classie.js"></script>
<script src="assets/js/jquery.nicescroll.js"></script>
<script src="assets/js/scripts.js"></script>
<script src="assets/js/bootstrap.js"> </script>
</body>
</html>
<?php
    require_once "header.php";
    $getCategories = $db->select()->from('categories')->where('parent_id','!=','0')->orderBy('title','ASC');
    $getCategories = $getCategories->execute();
    $getCategories = $getCategories->fetchAll(PDO::FETCH_ASSOC);
?>
<div id="page-wrapper">
    <div class="main-page">
        <div class="row">
            <h1>Brands</h1>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <?php require_once "flash_message.php" ?>
                <div class="panel panel-info">
                    <div class="panel-heading">
                        Add Brand
                    </div>
                    <div class="panel-body">
                        <form method="post" action="<?php echo $_SERVER['PHP_SELF']?>" enctype="multipart/form-data">
                            <input type="hidden" name="add_brand" value="1">
                            <div class="form-body">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="control-label">Categories<span class="text-danger">*</span></label>
                                            <select name="category[]" class="form-control" multiple>
                                                <option value=""> -- Select Category -- </option>
                                                <?php
                                                    if(!empty($getCategories)){
                                                        foreach($getCategories as $category){
                                                            echo '<option value="'. $category['id'] .'">'. $category['title'] .'</option>';
                                                        }
                                                    }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                    <!--/span-->
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="control-label">Title<span class="text-danger">*</span></label>
                                            <input type="text" name="title" class="form-control">
                                        </div>
                                    </div>
                                    <!--/span-->
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="control-label">Image<span class="text-danger">*</span></label>
                                            <input type="file" name="file" class="form-control">
                                        </div>
                                    </div>
                                    <!--/span-->
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="control-label">Status<span class="text-danger"></span></label>
                                            <select name="status" class="form-control">
                                                <option value="1">Active</option>
                                                <option value="0">In-Active</option>
                                            </select>
                                        </div>
                                    </div>
                                    <!--/span-->
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="control-label">Description<span class="text-danger"></span></label>
                                            <textarea name="description" class="form-control"></textarea>
                                        </div>
                                    </div>
                                    <!--/span-->
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                         <button type="submit" class="btn btn-success"> <i class="fa fa-check"></i> Save</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
require_once "footer.php";
?>

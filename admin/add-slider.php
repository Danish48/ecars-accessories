<?php
require_once "header.php";
?>
<div id="page-wrapper">
    <div class="main-page">
        <div class="row">
            <h1>Home Slider</h1>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <?php require_once "flash_message.php" ?>
                <div class="panel panel-info">
                    <div class="panel-heading">
                        Add Slider
                    </div>
                    <div class="panel-body">
                        <form method="post" action="<?php echo $_SERVER['PHP_SELF']?>" enctype="multipart/form-data">
                            <input type="hidden" name="add_slider" value="1">
                            <div class="form-body">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="control-label">Title<span class="text-danger"></span></label>
                                            <input type="text" name="title" class="form-control">
                                        </div>
                                    </div>
                                    <!--/span-->
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="control-label">Image<span class="text-danger">*</span></label>
                                            <input type="file" name="file" class="form-control">
                                        </div>
                                    </div>
                                    <!--/span-->
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="control-label">Link<span class="text-danger"></span></label>
                                            <input type="text" name="link" class="form-control">
                                        </div>
                                    </div>
                                    <!--/span-->
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="control-label">Description<span class="text-danger"></span></label>
                                            <textarea name="description" class="form-control"></textarea>
                                        </div>
                                    </div>
                                    <!--/span-->
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                         <button type="submit" class="btn btn-success"> <i class="fa fa-check"></i> Save</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
require_once "footer.php";
?>

<?php
    require_once "header.php";
    $getVendors   = $db->select()->from('users')->where('id','=',$getUser['id'])->where('role','=','user');
    $getVendors   = $getVendors->execute();
    $getVendors   = $getVendors->fetchAll(PDO::FETCH_ASSOC);
?>
<div id="page-wrapper">
    <div class="main-page">
        <div class="row">
            <h1>Users</h1>
        </div>
        <div class="row">
            <?php require_once "flash_message.php" ?>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        User Listing
                    </div>
                    <div class="panel-body">
                        <table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Fullname</th>
                                <th>Email</th>
                                <th>Mobile</th>
                                <th>Status</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                                if(!empty($getVendors)) {
                                    $index = '';
                                    foreach($getVendors as $vendor){
                                    $index++;
                             ?>
                                    <tr>
                                        <td><?php echo $index ?></td>
                                        <td><?php echo $vendor['first_name'] . ' ' . $vendor['last_name'] ?></td>
                                        <td><?php echo $vendor['email']  ?></td>
                                        <td><?php echo getUserMetaValue($db,$vendor['id'],'_mobile') ?></td>
                                        <td><?php echo $vendor['status'] == '1' ? '<a class="btn btn-sm btn-success">Active</a>' : '<a class="btn btn-sm btn-danger">In-active</a>' ?></td>
                                        <td><?php echo date('d-m-Y',strtotime($vendor['created_at'])); ?></td>
                                        <td><a class="delete_record" data-href="<?php echo $_SERVER['PHP_SELF'] . '?delete_record=true&location=vendors.php&table=users&id=' . $vendor['id'] ?>"><i class="fa fa-trash-o"></i></a></td>
                                    </tr>
                            <?php
                                    }
                                }
                            ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
require_once "footer.php";
?>

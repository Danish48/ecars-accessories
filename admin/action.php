<?php

require_once "db.php";

// logout
if(isset($_GET['logout'])){
    if (isset($_SERVER['HTTP_COOKIE'])) { // remove all cookie
        $cookies = explode(';', $_SERVER['HTTP_COOKIE']);
        foreach($cookies as $cookie) {
            $parts = explode('=', $cookie);
            $name = trim($parts[0]);
            setcookie($name, '', time()-1000); // remove cookie
            setcookie($name, '', time()-1000, '/'); // remove cookie
        }
    }
    session_destroy();
    header("Location:login.php");
    die;
}

// get loggedIn user
if (isset($_SERVER['HTTP_COOKIE'])) {
    if(isset($_COOKIE['current_user'])){
        $user_id = $_COOKIE['current_user'];
        $getUserStatement   = $db->select()->from('users')->where('id','=',$user_id);
        $userStatement      = $getUserStatement->execute();
        $getUser            = $userStatement->fetch();
    }
}

//account setting
if(isset($_POST['account_setting'])){
    $first_name  = $_POST['first_name'];
    $last_name   = $_POST['last_name'];
    $email       = $_POST['email'];
    $user_meta   = $_POST['user_meta'];
    if($first_name == '' || $last_name == '' || $email == '' || $user_meta == ''){
        $msg->error('Please fill in all require fields');
        header("Location:account-setting.php");
        die();
    }else{
        $user_data = [
            'first_name' => $first_name,
            'last_name'  => $last_name,
            'email'      => $email
        ];
        $updateStatement = $db->update($user_data)
                                ->table('users')
                                ->where('id', '=', $getUser['id']);
        $affectedRows = $updateStatement->execute();
        //update meta_value
        foreach($user_meta as $key => $value){
            $updateStatement = $db->update(array('meta_description' => $value))
                                ->table('user_meta')
                                ->where('user_id', '=', $getUser['id'])->where('meta_key','=','_' . $key);
            $affectedRows = $updateStatement->execute();
        }
        $msg->success("Account setting has been updated successfully.");
        header("Location:account-setting.php");
        die();
    }
}

//change password
if(isset($_POST['password_reset'])){
    $new_password     = $_POST['new_password'];
    $confirm_password = $_POST['confirm_password'];
    if($new_password == '' || $confirm_password == ''){
        $msg->error('Please fill in all require fields');
        header("Location:account-setting.php");
        die();
    }else if($new_password != $confirm_password){
        $msg->error('New password and confirm password does not match');
        header("Location:account-setting.php");
        die();
    }else{
        $updateStatement = $db->update(array('password' => md5($new_password)))
                            ->table('users')
                            ->where('id', '=', $getUser['id']);
        $affectedRows = $updateStatement->execute();
        $msg->success("Password has been updated successfully.");
        header("Location:account-setting.php");
        die();
    }
}

//add brand
if(isset($_POST['add_brand'])){
    $title       = $_POST['title'];
    $image       = $_FILES['file'];
    $status      = $_POST['status'];
    $description = $_POST['description'];
    $category    = $_POST['category'];
    if($title == '' || empty($image['name']) || empty($category)){
        $msg->error('Please fill in all require fields');
        header("Location:add-brand.php");
        die();
    }else{
        $file_size  = $_FILES['file']['size'];
        $file_tmp   = $_FILES['file']['tmp_name'];
        $file_type  = $_FILES['file']['type'];
        $file_ext   = strtolower(end(explode('.',$_FILES['file']['name'])));
        $expensions = array("jpeg","jpg","png");

        if(in_array($file_ext,$expensions)=== false){
            $msg->error("extension not allowed, please choose a JPEG or PNG file.");
            header("Location:add-brand.php");
            die();
        }
        if($file_size > 2097152){
            $msg->error("File size must be excately 2 MB.");
            header("Location:add-brand.php");
            die();
        }
        $new_file_name = time() . '.' .$file_ext;
        move_uploaded_file($file_tmp,"../_uploads/".$new_file_name);
        $insertStatement = $db->insert(array('user_id', 'title', 'slug','image','status','description'))
                            ->into('brands')
                            ->values(array($getUser['id'], $title, create_slug($title),$new_file_name,$status,$description ));
        $insertId = $insertStatement->execute(true);
        // brand category mapping
        foreach($category as $c){
            $insertStatement = $db->insert(array('category_id', 'brand_id'))
                                ->into('brand_category')
                                ->values(array($c, $insertId));
            $insertStatement->execute(true);
        }

        $msg->success("Brand has been added successfully.");
        header("Location:brands.php");
        die();
    }
}

//add category
if(isset($_POST['add_category'])){

    ini_set('display_errors', 1);
    ini_set('display_startup_errors', 1);
    error_reporting(E_ALL);

    $title       = $_POST['title'];
    $parent      = $_POST['parent_category'];
    $status      = $_POST['status'];
    $description = $_POST['description'];
    if($title == ''){
        $msg->error('Please fill in all require fields');
        header("Location:add-category.php");
        die();
    }else{
        $category_data = [
            'user_id'     => $getUser['id'],
            'title'       => $_POST['title'],
            'slug'        => create_slug($_POST['title']),
            'parent_id'   => (int)!empty($parent) ? $parent : 0,
            'status'      => $status,
            'description' => $description
        ];
        $insertStatement = $db->insert(array('user_id', 'title', 'slug','parent_id','status','description'))
                            ->into('categories')
                            ->values(array($getUser['id'], $title, create_slug($title),$parent,$status,$description ));
        $insertId = $insertStatement->execute(false);
        $msg->success("Category has been added successfully.");
        header("Location:categories.php");
        die();
    }
}

//add slider
if(isset($_POST['add_slider'])){
    $title       = $_POST['title'];
    $image       = $_FILES['file'];
    $link      = $_POST['link'];
    $description = $_POST['description'];
    if(empty($image['name'])){
        $msg->error('Please fill in all require fields');
        header("Location:add-slider.php");
        die();
    }else{
        $file_size  = $_FILES['file']['size'];
        $file_tmp   = $_FILES['file']['tmp_name'];
        $file_type  = $_FILES['file']['type'];
        $file_ext   = strtolower(end(explode('.',$_FILES['file']['name'])));
        $expensions = array("jpeg","jpg","png");

        if(in_array($file_ext,$expensions)=== false){
            $msg->error("extension not allowed, please choose a JPEG or PNG file.");
            header("Location:add-slider.php");
            die();
        }
        if($file_size > 2097152){
            $msg->error("File size must be excately 2 MB.");
            header("Location:add-slider.php");
            die();
        }
        $new_file_name = time() . '.' .$file_ext;
        move_uploaded_file($file_tmp,"../_uploads/".$new_file_name);
        $insertStatement = $db->insert(array('title','image','link','description'))
            ->into('slider')
            ->values(array($title,$new_file_name,$link,$description ));
        $insertId = $insertStatement->execute(false);
        $msg->success("Slider has been added successfully.");
        header("Location:slider.php");
        die();
    }
}

//delete record
if(isset($_GET['delete_record'])){
    $id       = $_GET['id'];
    $table    = $_GET['table'];
    $location = $_GET['location'];
    $deleteStatement = $db->delete()->from($table)->where('id', '=', $id);
    $affectedRows = $deleteStatement->execute();
    $msg->success("Record has been deleted successfully.");
    header("Location:" . $location);
    die();
}

function create_slug($string){
    $slug=preg_replace('/[^A-Za-z0-9-]+/', '-', $string);
    return $slug;
}

function getUserMetaValue($db,$user_id,$meta_key)
{
    $getUserMeta   = $db->select()->from('user_meta')->where('user_id','=',$user_id)->where('meta_key','=',$meta_key);
    $getUserMeta   = $getUserMeta->execute();
    $getUserMeta   = $getUserMeta->fetch();
    if(count($getUserMeta)){
        return $getUserMeta['meta_description'];
    }else{
        return false;
    }
}
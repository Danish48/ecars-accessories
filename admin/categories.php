<?php
    require_once "header.php";
    $getCatStatement   = $db->select()->from('categories')->where('parent_id','=',0);
    $catStatement      = $getCatStatement->execute();
    $getCategories     = $catStatement->fetchAll(PDO::FETCH_ASSOC);
?>
<div id="page-wrapper">
    <div class="main-page">
        <div class="row">
            <h1>Categories</h1>
        </div>
        <div class="row">
            <?php require_once "flash_message.php" ?>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        Category Listing
                        <a href="add-category.php" class="btn btn-info pull-right" style="margin-top: -10px;">Add Category</a>
                    </div>
                    <div class="panel-body">
                        <table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Parent</th>
                                <th>Title</th>
                                <th>status</th>
                                <th>created_at</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                                if(!empty($getCategories)) {
                                    $index = '';
                                    foreach($getCategories as $category){
                                    $index++;
                             ?>
                                    <tr>
                                        <td><?php echo $index ?></td>
                                        <td><?php echo !empty($category['parent_title']) ? $category['parent_title'] : '' ?></td>
                                        <td><?php echo $category['title'] ?></td>
                                        <td><?php echo $category['status'] == '1' ? '<a class="btn btn-sm btn-success">Active</a>' : '<a class="btn btn-sm btn-danger">In-active</a>' ?></td>
                                        <td><?php echo date('d-m-Y',strtotime($category['created_at'])); ?></td>
                                        <td><a class="delete_record" data-href="<?php echo $_SERVER['PHP_SELF'] . '?delete_record=true&location=categories.php&table=categories&id=' . $category['id'] ?>"><i class="fa fa-trash-o"></i></a></td>
                                    </tr>
                            <?php
                                $getChilds   = $db->select()->from('categories')->where('parent_id','=',$category['id']);
                                $getChilds   = $getChilds->execute();
                                $getChilds   = $getChilds->fetchAll(PDO::FETCH_ASSOC);

                               if(count($getChilds)){
                                    $child_index = '';
                                    foreach($getChilds as $child){
                                    $child_index++;
                            ?>
                                        <tr>
                                            <td><?php echo $index . '.' . $child_index ?></td>
                                            <td><?php echo $category['title'] ?></td>
                                            <td><?php echo $child['title'] ?></td>
                                            <td><?php echo $child['status'] == '1' ? '<a class="btn btn-sm btn-success">Active</a>' : '<a class="btn btn-sm btn-danger">In-active</a>' ?></td>
                                            <td><?php echo date('d-m-Y',strtotime($child['created_at'])); ?></td>
                                            <td><a class="delete_record" data-href="<?php echo $_SERVER['PHP_SELF'] . '?delete_record=true&location=categories.php&table=categories&id=' . $child['id'] ?>"><i class="fa fa-trash-o"></i></a></td>
                                        </tr>
                            <?php
                                            }
                                        }
                                    }
                                }
                            ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
require_once "footer.php";
?>

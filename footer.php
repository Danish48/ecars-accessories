<!--Footer Start-->
<footer id="footer">
    <div class="fpart-second">
        <div class="container">
            <div id="powered" class="clearfix">
                <div class="powered_text pull-left flip">
                    <p>Marketshop Ecommerce Template © 2016 | Template By <a href="assets/http://harnishdesign.net/" target="_blank">Harnish Design</a></p>
                </div>
                <div class="social pull-right flip"> <a href="assets/#" target="_blank"> <img data-toggle="tooltip" src="assets/image/socialicons/facebook.png" alt="Facebook" title="Facebook"></a> <a href="assets/#" target="_blank"> <img data-toggle="tooltip" src="assets/image/socialicons/twitter.png" alt="Twitter" title="Twitter"> </a> <a href="assets/#" target="_blank"> <img data-toggle="tooltip" src="assets/image/socialicons/google_plus.png" alt="Google+" title="Google+"> </a> <a href="assets/#" target="_blank"> <img data-toggle="tooltip" src="assets/image/socialicons/pinterest.png" alt="Pinterest" title="Pinterest"> </a> <a href="assets/#" target="_blank"> <img data-toggle="tooltip" src="assets/image/socialicons/rss.png" alt="RSS" title="RSS"> </a> </div>
            </div>
            <div class="bottom-row">
                <div class="custom-text text-center"> <img alt="" src="assets/image/logo-small.png">
                    <p>This is a CMS block. You can insert any content (HTML, Text, Images) Here. Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
                </div>
            </div>
        </div>
    </div>
    <div id="back-top"><a data-toggle="tooltip" title="Back to Top" href="assets/javascript:void(0)" class="backtotop"><i class="fa fa-chevron-up"></i></a></div>
</footer>
</div>
<!-- JS Part Start-->
<script type="text/javascript" src="assets/js/jquery-2.1.1.min.js"></script>
<script type="text/javascript" src="assets/js/bootstrap/js/bootstrap.min.js"></script>
<script type="text/javascript" src="assets/js/jquery.easing-1.3.min.js"></script>
<script type="text/javascript" src="assets/js/jquery.dcjqaccordion.min.js"></script>
<script type="text/javascript" src="assets/js/owl.carousel.min.js"></script>
<script type="text/javascript" src="assets/js/jquery.elevateZoom-3.0.8.min.js"></script>
<script type="text/javascript" src="assets/js/swipebox/lib/ios-orientationchange-fix.js"></script>
<script type="text/javascript" src="assets/js/swipebox/src/js/jquery.swipebox.min.js"></script>
<script type="text/javascript" src="assets/js/custom.js"></script>
<!-- JS Part End-->
</body>
</html>
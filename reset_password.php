<?php
  if(isset($_COOKIE['current_user'])){
    header("Location:index.php");
    die();
  }
  if(!isset($_GET['email']) && !isset($_GET['id'])){
    header("Location:login.php");
    die();
  }
  require_once "header.php";
  $email = urldecode($_GET['email']);
  $id    = $_GET['id'];
  $getUserStatement   = $db->select()->from('users')->where('email','=',$email)->where('role','=','user');
  $userStatement      = $getUserStatement->execute();
  $getUser            = $userStatement->fetch();
  if(empty($getUser)){
    $msg->error('Invalid request.');
    header("Location:login.php");
    die();
  }
?>
  <div id="container">
    <div class="container">
      <div class="row">
        <!--Middle Part Start-->
        <?php require_once "flash_message.php"; ?>
        <form method="post">
          <input type="hidden" name="email" value="<?php echo $email ?>">
          <input type="hidden" name="id" value="<?php echo $id ?>">
          <input type="hidden" name="reset_password" value="1">
          <div id="content" class="col-sm-9">
          <h1 class="title">Reset Password</h1>
          <div class="row">
            <div class="col-sm-6">
                <div class="form-group">
                  <label class="control-label" for="input-email">New Password</label>
                  <input type="password" name="new_password" value="" placeholder="New Password" id="input-email" class="form-control" />
                </div>
                <div class="form-group">
                  <label class="control-label" for="input-password">Confirm Password</label>
                  <input type="password" name="confirm_password" value="" placeholder="Confirm Password" id="input-password" class="form-control" />
                  <br />
                </div>
                <input type="submit" value="Submit" class="btn btn-primary" />
            </div>
          </div>
        </div>
        </form>  
      </div>
    </div>
  </div>
<?php
  require_once "footer.php";
?>

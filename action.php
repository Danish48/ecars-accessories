<?php

require_once "db.php";
require_once "mail.php";

// logout
if(isset($_GET['logout'])){
    if (isset($_SERVER['HTTP_COOKIE'])) { // remove all cookie
        $cookies = explode(';', $_SERVER['HTTP_COOKIE']);
        foreach($cookies as $cookie) {
            $parts = explode('=', $cookie);
            $name = trim($parts[0]);
            setcookie($name, '', time()-1000); // remove cookie
            setcookie($name, '', time()-1000, '/'); // remove cookie
        }
    }
    session_destroy();
    header("Location:index.php");
    die;
}

// get loggedIn user
if (isset($_SERVER['HTTP_COOKIE'])) {
    if(isset($_COOKIE['current_user'])){
        $user_id = $_COOKIE['current_user'];
        $getUserStatement   = $db->select()->from('users')->where('id','=',$user_id);
        $userStatement      = $getUserStatement->execute();
        $getUser            = $userStatement->fetch();
    }
}

//register
if(isset($_POST['register'])){
    $first_name       = $_POST['first_name'];
    $last_name        = $_POST['last_name'];
    $email            = $_POST['email'];
    $password         = $_POST['password'];
    $confirm_password = $_POST['confirm_password'];
    $role             = $_POST['role'];

    if($first_name == '' || $last_name == '' || $email == '' || $password == '' || $confirm_password == '' || $role == ''){
        $msg->error('Please fill in all require fields');
        header("Location:register.php");
        die();
    }else if($password != $confirm_password){
        $msg->error('Password and confirm password does not match.');
        header("Location:register.php");
        die();
    }else{
        $insertStatement = $db->insert(array('first_name', 'last_name', 'email','password','role','status','created_at'))
                                ->into('users')
                                ->values(array($first_name, $last_name, $email,md5($password),$role,'0',date('Y-m-d H:i:s')));
        $insertId = $insertStatement->execute(true);
        $email_data = [
            'name'               => $first_name,
            'mail_to'            => $_POST['email'],
            'subject'            => 'Verify your email account',
            'body_identifier'    => 'registration',
            'link'               => APP_URL . 'action.php?verfiy_account=true&id=' . $insertId
        ];
        sendMail($email_data);
        $msg->success('Your account has been created successfully.Verification email has been sent to your email-address.');
        if($role == 'vendor'){
            header("Location:register.php");
            die();
        }else{
            header("Location:login.php");
            die();
        }
    }
}

//verify account
if(isset($_GET['verfiy_account'])){
    $updateStatement = $db->update(array('status' => '1'))
                            ->table('users')
                            ->where('id', '=', $_GET['id']);
    $affectedRows = $updateStatement->execute();
    $msg->success('Your account has been verified successfully.');
    header("Location:login.php");
    die();
}

//login
if(isset($_POST['login'])){
    $email            = $_POST['email'];
    $password         = $_POST['password'];
    if($email == '' || $password == ''){
        $msg->error('Please fill in all require fields.');
        header("Location:register.php");
        die();
    }else{
        $getUserStatement   = $db->select()->from('users')->where('email','=',$email)->where('password','=',md5($password))->where('status','=','1')->where('role','=','user');
        $userStatement      = $getUserStatement->execute();
        $getUser            = $userStatement->fetch();
        if(empty($getUser)){
            $msg->error('Invalid credentials');
            header("Location:login.php");
            die();
        }else{
            setcookie("current_user", $getUser['id'], time() + (86400 * 30), "/");
            header("Location:index.php");
        }
    }
}
//forgot password
if(isset($_POST['forgot_password'])){
    $email = $_POST['email'];
    if($email == ''){
        $msg->error('Email field is required');
        header("Location:forgot-password.php");
        die();
    }else{
        $getUserStatement   = $db->select()->from('users')->where('email','=',$email)->where('role','!=','admin');
        $userStatement      = $getUserStatement->execute();
        $getUser            = $userStatement->fetch();
        if(empty($getUser)){
            $msg->error('Invalid email');
            header("Location:forgot-password.php");
            die();
        }else{
            $email_data = [
                'name'               => $getUser['first_name'],
                'mail_to'            => $email,
                'subject'            => '['. APP_NAME .'] Reset your password',
                'body_identifier'    => 'reset_password',
                'link'               => APP_URL . 'reset_password.php?email=' . urlencode($email) . '&id=' . $getUser['id']
            ];
            sendMail($email_data);
            $msg->success('Reset password link has been send on your email. Kindly check your inbox and continue.');
            header("Location:login.php");
        }
    }
}
//reset password
if(isset($_POST['reset_password'])){
    $email            = $_POST['email'];
    $id               = $_POST['id'];
    $password         = $_POST['new_password'];
    $confirm_password = $_POST['confirm_password'];
    if($email == '' || $id == '' || $password == '' || $confirm_password == ''){
        $msg->error('Please fill in all require fields');
        header("Location:reset_password.php?email=" . urlencode($email) . '&id=' . $id);
        die();
    }else if($password != $confirm_password){
        $msg->error('New Password and confirm password does not match');
        header("Location:reset_password.php?email=" . urlencode($email) . '&id=' . $id);
        die();
    }else{
        $updateStatement = $db->update(array('password' => md5($password)))
            ->table('users')
            ->where('id', '=', $id)->where('email','=',$email);
        $affectedRows = $updateStatement->execute();
        $msg->success('Password has been updated successfully.');
        header("Location:login.php");
        die();
    }
}

//change password
if(isset($_POST['change_password'])){
    $new_password     = $_POST['new_password'];
    $confirm_password = $_POST['confirm_password'];
    if($new_password == '' || $confirm_password == ''){
        $msg->error('Please fill in all require fields');
        header("Location:account_setting.php");
        die();
    }else if($new_password != $confirm_password){
        $msg->error('New password and confirm password does not match');
        header("Location:account_setting.php");
        die();
    }else{
        $updateStatement = $db->update(array('password' => md5($new_password)))
            ->table('users')
            ->where('id', '=', $getUser['id']);
        $affectedRows = $updateStatement->execute();
        $msg->success("Password has been updated successfully.");
        header("Location:account_setting.php");
        die();
    }
}

//account setting
if(isset($_POST['account_setting'])){
    $first_name  = $_POST['first_name'];
    $last_name   = $_POST['last_name'];
    $user_meta   = $_POST['user_meta'];
    if($first_name == '' || $last_name == '' || empty($user_meta)){
        $msg->error('Please fill in all require fields');
        header("Location:account_setting.php");
        die();
    }else{
        $user_data = [
            'first_name' => $first_name,
            'last_name'  => $last_name
        ];
        $updateStatement = $db->update($user_data)
                                ->table('users')
                                ->where('id', '=', $getUser['id']);
        $affectedRows = $updateStatement->execute();
        //update meta_value
        foreach($user_meta as $key => $value){
            //check meta value
            $checkUserMeta = $db->select()->from('user_meta')->where('user_id','=',$user_id)->where('meta_key','=','_' . $key);
            $checkUserMeta = $checkUserMeta->execute();
            $checkUserMeta = $checkUserMeta->fetch();
            //insert
            if(empty($checkUserMeta)){
                $insertStatement = $db->insert(array('user_id', 'meta_key', 'meta_description'))
                    ->into('user_meta')
                    ->values(array($getUser['id'], '_' . $key, $value));
                $insertId = $insertStatement->execute(false);
            }else{
                //update
                $updateStatement = $db->update(array('meta_description' => $value))
                    ->table('user_meta')
                    ->where('user_id', '=', $getUser['id'])->where('meta_key','=','_' . $key);
                $affectedRows = $updateStatement->execute();
            }
        }
        $msg->success("Account setting has been updated successfully.");
        header("Location:account_setting.php");
        die();
    }
}

//change password
if(isset($_POST['password_reset'])){
    $new_password     = $_POST['new_password'];
    $confirm_password = $_POST['confirm_password'];
    if($new_password == '' || $confirm_password == ''){
        $msg->error('Please fill in all require fields');
        header("Location:account-setting.php");
        die();
    }else if($new_password != $confirm_password){
        $msg->error('New password and confirm password does not match');
        header("Location:account-setting.php");
        die();
    }else{
        $updateStatement = $db->update(array('password' => md5($new_password)))
                            ->table('users')
                            ->where('id', '=', $getUser['id']);
        $affectedRows = $updateStatement->execute();
        $msg->success("Password has been updated successfully.");
        header("Location:account-setting.php");
        die();
    }
}
//contact us
if(isset($_POST['contact_us'])){
    $name    = $_POST['name'];
    $email   = $_POST['email'];
    $enquiry = $_POST['enquiry'];
    if($name == '' || $email == '' || $enquiry == ''){
        $msg->error("Please fill in all require fields.");
        header("Location:contact-us.php");
        die();
    }else{
        $email_data = [
            'name'               => APP_NAME,
            'mail_to'            => ADMIN_EMAIL,
            'subject'            => '['. APP_NAME .'] Enquiry',
            'body_identifier'    => 'contact_us',
            'link'               => ''
        ];
        sendMail($email_data,$enquiry);
        $msg->success("Message has been sent successfully.");
        header("Location:contact-us.php");
        die();
    }
}

//delete record
if(isset($_GET['delete_record'])){
    $id       = $_GET['id'];
    $table    = $_GET['table'];
    $location = $_GET['location'];
    $deleteStatement = $db->delete()->from($table)->where('id', '=', $id);
    $affectedRows = $deleteStatement->execute();
    $msg->success("Record has been deleted successfully.");
    header("Location:" . $location);
    die();
}

function create_slug($string){
    $slug=preg_replace('/[^A-Za-z0-9-]+/', '-', $string);
    return $slug;
}

function getUserMetaValue($db,$user_id,$meta_key)
{
    $getUserMeta   = $db->select()->from('user_meta')->where('user_id','=',$user_id)->where('meta_key','=',$meta_key);
    $getUserMeta   = $getUserMeta->execute();
    $getUserMeta   = $getUserMeta->fetch();
    if(count($getUserMeta)){
        return $getUserMeta['meta_description'];
    }else{
        return false;
    }
}
<?php
  if(!isset($_COOKIE['current_user'])){
    header("Location:index.php");
    die();
  }
  require_once "header.php";
?>
  <div id="container">
    <div class="container">
      <div class="row">
        <!--Middle Part Start-->
        <?php require_once "flash_message.php"; ?>
          <div id="content" class="col-lg-9 col-offset-lg-3 col-md-9 col-offset-md-3  col-sm-12">
          <h1 class="title">Account Setting</h1>
          <div class="row">
            <div class="col-sm-6">
              <form method="post">
                <input type="hidden" name="account_setting" value="1">
                  <div class="form-group">
                    <label class="control-label" for="input-email">First Name<span class="text-danger">*</span></label>
                    <input type="text" name="first_name" value="<?php echo $getUser['first_name'] ?>" placeholder="First Name" id="input-email" class="form-control" />
                  </div>
                  <div class="form-group">
                    <label class="control-label" for="input-email">Last Name<span class="text-danger">*</span></label>
                    <input type="text" name="last_name" value="<?php echo $getUser['last_name'] ?>" placeholder="Last Name" id="input-email" class="form-control" />
                  </div>
                  <div class="form-group">
                    <label class="control-label" for="input-email">Email<span class="text-danger">*</span></label>
                    <input disabled type="email" name="email" value="<?php echo $getUser['email'] ?>" placeholder="Email" id="input-email" class="form-control" />
                  </div>
                  <div class="form-group">
                    <label class="control-label" for="input-email">Mobile<span class="text-danger">*</span></label>
                    <input type="text" name="user_meta[mobile]" value="<?php echo getUserMetaValue($db,$getUser['id'],'_mobile') ?>" placeholder="Mobile" id="input-email" class="form-control" />
                  </div>
                  <div class="form-group">
                    <label class="control-label" for="input-email">Address<span class="text-danger">*</span></label>
                    <input type="text" name="user_meta[address]" value="<?php echo getUserMetaValue($db,$getUser['id'],'_address') ?>" placeholder="Address" id="input-email" class="form-control" />
                  </div>
                  <input type="submit" value="Submit" class="btn btn-primary" />
              </form>
            </div>
            <div class="col-sm-6">
              <form method="post">
                <input type="hidden" name="change_password" value="1">
                <div id="content" style="margin-top: 50px;">
                  <h1 class="title">Change Password</h1>
                  <div class="row">
                    <div class="col-sm-6">
                      <div class="form-group">
                        <label class="control-label" for="input-email">New Password<span class="text-danger">*</span></label>
                        <input type="password" name="new_password" value="" placeholder="New Password" id="input-email" class="form-control" />
                      </div>
                      <div class="form-group">
                        <label class="control-label" for="input-email">Confirm Password<span class="text-danger">*</span></label>
                        <input type="password" name="confirm_password" value="" placeholder="Confirm Password" id="input-email" class="form-control" />
                      </div>
                      <input type="submit" value="Submit" class="btn btn-primary" />
                    </div>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
<?php
  require_once "footer.php";
?>

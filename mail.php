<?php
// Import PHPMailer classes into the global namespace
// These must be at the top of your script, not inside a function
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

//Load composer's autoloader
require_once 'vendor/autoload.php';
require_once 'constants.php';

function sendMail($data,$message = '')
{
    $mail = new PHPMailer(true);                              // Passing `true` enables exceptions
    try {
        //Server settings
        $mail->SMTPDebug = 2;                                 // Enable verbose debug output
        $mail->isSMTP();                                      // Set mailer to use SMTP
        $mail->Host = 'smtp.gmail.com';                        // Specify main and backup SMTP servers
        $mail->SMTPAuth = true;                               // Enable SMTP authentication
        $mail->Username = 'carstylo15@gmail.com';                 // SMTP username
        $mail->Password = 'bzozqdmgobevtycd';                           // SMTP password
        $mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
        $mail->Port = 587;                                    // TCP port to connect to
        //Recipients
        $mail->setFrom('no-reply@ecars.com', 'E-Cars');
        $mail->addAddress($data['mail_to']);     // Add a recipient
        if(isset($data['mail_cc'])){
            $mail->addCC('cc@example.com');
        }
        if(isset($data['mail_cc'])){
            if(count($data['attachment']) && is_array($data['attachment'])){
                foreach($data['attachment'] as $attachment){
                    $mail->addAttachment($attachment);         // Add attachments
                }
            }   
        }
        //Content
        $mail->isHTML(true);                                  // Set email format to HTML
        $mail->Subject = $data['subject'];
        $mail->Body    = $data['body_identifier'] == 'contact_us' ? $message : getBody($data['body_identifier'],$data['link'],$data['name']);
        $mail->send();
    } catch (Exception $e) {
        echo 'Message could not be sent. Mailer Error: ', $mail->ErrorInfo;
    }   
}

function getBody($identifier,$link,$name)
{
    $body = '';
    if($identifier == 'registration'){
        $body = "Dear $name, <br /><br />
                 Welcome to ". APP_NAME ." - Your account has been created successfully. We need you to verify your email address. 
                 Please click on the link below: <br /><br />
                 <a href='". $link ."'>Verify Now</a><br /><br /><br /><br />
                 Team ". APP_NAME;
    }
    if($identifier == 'reset_password'){
        $body = "Dear $name, <br /><br />
                 We heard that you lost your ". APP_NAME ." password. Sorry about that!. <br /><br />
                 But don’t worry! You can use the following link to reset your password: <br /><br />
                 <a href='". $link ."'>Reset Password</a><br /><br /><br /><br />
                 Team ". APP_NAME;
    }
    return $body;
}